angular
	.module('app', [
		'ui.router',
		'ui.bootstrap',
		'ngAnimate',
		'angularSlideables',
		'ngStorage',
		'facebook',
		'google.places',
		'infinite-scroll',
		'btford.socket-io'
		// 'slick',
		// 'rzModule',
		// 'btford.socket-io',
		// '720kb.socialshare',
	])
	.config(function(FacebookProvider) {
		FacebookProvider.init('278381285923341');
	});