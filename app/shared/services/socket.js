(function() {
	'use strict';
	angular
	.module('app')
	.factory('socket', socket);


	function socket(socketFactory) {
		return socketFactory({
			ioSocket: io.connect('http://54.200.74.162:3000')
		});
	};
})();