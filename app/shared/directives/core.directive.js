(function() {
	'use strict';
	angular
	.module('app')
	.directive('onFinishRender', function($timeout) {
		return {
			restrict: 'A',
			link: function(scope, element, attr) {
				if (scope.$last === true) {
					$timeout(function() {
						scope.$emit(attr.onFinishRender);
					});
				}
			}
		}
	})
	.directive('onFinishRenderTwo', function($timeout) {
		return {
			restrict: 'A',
			link: function(scope, element, attr) {
				if (scope.$last === true) {
					$timeout(function() {
						scope.$emit(attr.onFinishRenderTwo);
					});
				}
			}
		}
	})
	.directive('onFinishRenderThree', function($timeout) {
		return {
			restrict: 'A',
			link: function(scope, element, attr) {
				if (scope.$last === true) {
					$timeout(function() {
						scope.$emit(attr.onFinishRenderThree);
					});
				}
			}
		}
	})
	.directive('pnLoadingSpinner', function() {
		return {
			restrict: 'E',
			scope: {
				paddingTop: '@',
				paddingBottom: '@'
			},
			template:
			'<div style="padding-top: {{paddingTop}}; padding-bottom: {{paddingBottom}};">'+
			'	<div class="loading-spinner"></div>'+
			'</div>',
			controller: function($scope){
				$scope.paddingTop = (!$scope.paddingTop) ? '50px' : $scope.paddingTop;
				$scope.paddingBottom = (!$scope.paddingBottom) ? '50px' : $scope.paddingBottom;
			}
		}
	});
})();