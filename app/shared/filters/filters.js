(function() {
	'use strict';
	angular
	.module('app')
	.filter('parseDate1', function(){
		return function(date){
			var date = moment(date).format("MMMM DD YYYY, hh:mm A");
			return date;
		}
	})
	.filter('parseDate2', function(){
		return function(date){
			var date = moment(date).format("MMMM Do, YYYY");
			return date;
		}
	});

})();