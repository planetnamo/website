angular
	.module('app')
	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('home', {
				url: '/home',
				templateUrl: 'app/modules/home/home.html',
				controller: 'HomeController'
			})
			.state('signup', {
				url: '/signup',
				templateUrl: 'app/modules/signup/signup.html',
				controller: 'SignupController'
			})
			.state('buy', {
				url: '/buy',
				templateUrl: 'app/modules/buy/buy.html',
				controller: 'BuyController',
				params: {
					lotType: null
				}
			})
			.state('buyWithQuery', {
				url: '/buy/q=:query',
				templateUrl: 'app/modules/buy/buy.html',
				controller: 'BuyController'
			})
			.state('rent', {
				abstract: true,
				url: '/rent',
				templateUrl: 'app/modules/rent/rent.html'
			})
			.state('rent.products', {
				url: '/products',
				templateUrl: 'app/modules/rent/products/products.html',
				controller: 'RentProductsController'
			})
			.state('rent.details', {
				url: '/products/:id',
				templateUrl: 'app/modules/rent/details/details.html',
				controller: 'RentDetailsController'
			})
			.state('details', {
				abstract: true,
				url: '/details',
				templateUrl: 'app/modules/details/details.html'
				// controller: 'DetailsController'
			})
			.state('details.detailsDirectBuy', {
				url: '/direct-buy/:id',
				templateUrl: 'app/modules/details/direct-buy/direct-buy.html',
				controller: 'DetailsDirectBuyController'
			})
			.state('details.detailsClearance', {
				url: '/clearance/:id',
				templateUrl: 'app/modules/details/clearance/clearance.html',
				controller: 'DetailsClearanceController'
			})
			.state('details.detailsAuction', {
				url: '/auction/:id',
				templateUrl: 'app/modules/details/auction/auction.html',
				controller: 'DetailsAuctionController'
			})
			// .state('payEMD', {
			// 	url: '/pay-emd/:auctionId',
			// 	templateUrl: 'app/modules/details/auction/pay-emd.html',
			// 	controller: 'PayEMDController'
			// })
			.state('cart', {
				abstract: true,
				url: '/cart',
				templateUrl: 'app/modules/cart/cart.html'
				// controller: 'DetailsController'
			})
			// .state('cart.overview', {
			// 	url: '/overview',
			// 	templateUrl: 'app/modules/cart/overview/overview.html',
			// 	controller: 'CartOverviewController'
			// })
			.state('cart.delivery', {
				url: '/delivery',
				templateUrl: 'app/modules/cart/delivery/delivery.html',
				controller: 'CartDeliveryController',
				params: {
					lotId: null
				}
			})
			.state('cart.rental', {
				url: '/rental',
				templateUrl: 'app/modules/cart/rental/rental.html',
				controller: 'CartRentalController',
				params: {
					lotId: null,
					monthsToRent: null,
					rent: null,
					startRentalDate: null
				}
			})
			// .state('cart.pay', {
			// 	url: '/pay',
			// 	templateUrl: 'app/modules/cart/pay/pay.html'
			// 	// controller: 'CartPayController'
			// })
			.state('sell', {
				url: '/sell',
				templateUrl: 'app/modules/sell/sell.html'
				// controller: 'CartPayController'
			})
			.state('sellProduct', {
				abstract: true,
				url: '/sell-product/:category',
				templateUrl: 'app/modules/sell/sell-product.html',
				controller: 'SellProductController'
			})
			.state('sellProduct.step1', {
				url: '/',
				templateUrl: 'app/modules/sell/step1/sell-product-step-1.html',
				controller: 'SellProductStep1Controller'
			})
			.state('sellProduct.step2', {
				url: '/step2',
				templateUrl: 'app/modules/sell/step2/sell-product-step-2.html',
				controller: 'SellProductStep2Controller'
			})
			.state('sellProduct.step3', {
				url: '/step3',
				templateUrl: 'app/modules/sell/step3/sell-product-step-3.html',
				controller: 'SellProductStep3Controller'
			})
			.state('sellProduct.step4', {
				url: '/step4',
				templateUrl: 'app/modules/sell/step4/sell-product-step-4.html',
				controller: 'SellProductStep4Controller'
			})
			.state('sellProduct.step5', {
				url: '/step5',
				templateUrl: 'app/modules/sell/step5/sell-product-step-5.html',
				controller: 'SellProductStep5Controller'
			})
			.state('sellProduct.step6', {
				url: '/step6',
				templateUrl: 'app/modules/sell/step6/sell-product-step-6.html',
				controller: 'SellProductStep6Controller'
			})
			.state('sellProduct.step7', {
				url: '/step7',
				templateUrl: 'app/modules/sell/step7/sell-product-step-7.html',
				controller: 'SellProductStep7Controller'
			})
			.state('sellProduct.step8', {
				url: '/step8',
				templateUrl: 'app/modules/sell/step8/sell-product-step-8.html',
				controller: 'SellProductStep8Controller'
			})
			.state('sellProduct.step9', {
				url: '/step9',
				templateUrl: 'app/modules/sell/step9/sell-product-step-9.html',
				controller: 'SellProductStep9Controller'
			})
			.state('me', {
				abstract: true,
				url: '/me',
				templateUrl: 'app/modules/me/me.html',
				controller: 'MeController'
			})
			.state('me.profile', {
				url: '/profile',
				templateUrl: 'app/modules/me/profile/profile.html',
				controller: 'MeProfileController'
			})
			.state('me.wishlist', {
				url: '/wishlist',
				templateUrl: 'app/modules/me/wishlist/wishlist.html',
				controller: 'MeWishlistController'
			})
			.state('me.wallet', {
				url: '/wallet',
				templateUrl: 'app/modules/me/wallet/wallet.html',
				controller: 'MeWalletController'
			})
			.state('me.orders', {
				url: '/orders',
				templateUrl: 'app/modules/me/orders/orders.html',
				controller: 'MeOrderController'
			})
			.state('me.sales', {
				url: '/sales',
				templateUrl: 'app/modules/me/sales/sales.html',
				controller: 'MeSaleController'
			})
			.state('me.rentals', {
				url: '/rentals',
				templateUrl: 'app/modules/me/rentals/rentals.html',
				controller: 'MeRentalController'
			})
			.state('me.aggregate', {
				url: '/aggregate',
				templateUrl: 'app/modules/me/aggregate/aggregate.html',
				controller: 'MeAggregateController'
			})
			.state('me.auctionHall', {
				abstract: true,
				url: '/auction-hall',
				templateUrl: 'app/modules/me/auction-hall/auction-hall.html',
				controller: 'MeAuctionHallController'
			})
			.state('me.auctionHall.live', {
				url: '/live',
				templateUrl: 'app/modules/me/auction-hall/live/auction-hall-live.html',
				controller: 'MeAuctionHallLiveController'
			})
			.state('me.auctionHall.expired', {
				url: '/expired',
				templateUrl: 'app/modules/me/auction-hall/expired/auction-hall-expired.html',
				controller: 'MeAuctionHallExpiredController'
			})
			.state('me.auctionHall.won', {
				url: '/won',
				templateUrl: 'app/modules/me/auction-hall/won/auction-hall-won.html',
				controller: 'MeAuctionHallWonController'
			})
			.state('me.auctionHall.archive', {
				url: '/archive',
				templateUrl: 'app/modules/me/auction-hall/archive/auction-hall-archive.html',
				controller: 'MeAuctionHallArchiveController'
			})
			.state('contact', {
				url: '/contact-us',
				templateUrl: 'app/modules/contact/contact.html',
				controller: 'ContactController'
			})
			.state('about', {
				url: '/about-us',
				templateUrl: 'app/modules/about/about.html',
				controller: 'AboutController'
			})
			.state('compare', {
				abstract: true,
				url: '/compare',
				templateUrl: 'app/modules/compare/compare.html',
				controller: 'CompareController'
			})
			.state('compare.clearance', {
				url: '/clearance',
				templateUrl: 'app/modules/compare/clearance/clearance.html',
				controller: 'CompareClearanceController'
			})
			.state('compare.auction', {
				url: '/auction',
				templateUrl: 'app/modules/compare/auction/auction.html',
				controller: 'CompareAuctionController'
			})
			.state('compare.buy', {
				url: '/buy',
				templateUrl: 'app/modules/compare/buy/buy.html',
				controller: 'CompareBuyController'
			})
			.state('compare.rent', {
				url: '/rent',
				templateUrl: 'app/modules/compare/rent/rent.html',
				controller: 'CompareRentController'
			})
			.state('forgotPassword', {
				url: '/forgotPassword/reset/:email/:updateToken',
				templateUrl: 'app/modules/forgot-password/forgot-password.html',
				controller: 'ForgotPasswordController'
			});

		$urlRouterProvider.when('/rent', '/rent/products'); // Redirect in case user goes to the abstract state
		$urlRouterProvider.when('/sell-product', '/sell'); // Redirect in case user goes to the abstract state
		$urlRouterProvider.when('/me', '/me/profile'); // Redirect in case user goes to the abstract state
		$urlRouterProvider.when('/me/auction-hall', '/me/auction-hall/live'); // Redirect in case user goes to the abstract state
		$urlRouterProvider.otherwise('/home');
	}]);