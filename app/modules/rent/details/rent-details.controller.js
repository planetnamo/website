(function() {
	'use strict';
	angular
	.module('app')
	.controller('RentDetailsController', RentDetailsController);

	// RentDetailsController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	RentDetailsController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$timeout', 'network', 'authService'];

	// function RentDetailsController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function RentDetailsController($scope, $rootScope, $state, $stateParams, $timeout, network, authService) {

			$scope.navTabs = [
			{
				"name":"SPECIFICATIONS"	
			},
			{
				"name":"FEATURES"	
			},
			{
				"name":"WARRANTY"	
			},
			{
				"name":"T&Cs"	
			}
			];

			// For date picker 
			$scope.today = function() {
				$scope.dt = new Date();
				$scope.dtEnd = new Date();
			};
			$scope.today();

			$scope.clear = function() {
				$scope.dt = null;
			};

			$scope.dateOptions = {
				formatYear: 'yy',
				showWeeks: false,
				startingDay: 1
			};

			$scope.open1 = function() {
				$scope.popup1.opened = true;
			};

			$scope.popup1 = {
				opened: false
			};

			$scope.open2 = function() {
				$scope.popup2.opened = true;
			};

			$scope.popup2 = {
				opened: false
			};

			$scope.format = "yyyy-MM-dd";


			$scope.selectedNumberOfMonths = null;

			$scope.activeTabs = [];
			$scope.goToTab = function(tab){
				$scope.activeTabs = [];
				$scope.activeTabs[tab] = "active";
			};
			$scope.goToTab($scope.navTabs[0].name);
			
			$scope.selectedExtendPeriod = [];
			$scope.changeExtendPeriod = function(value){
				$scope.selectedExtendPeriod = [];
				$scope.selectedExtendPeriod[value] = "selected";
			};

			$scope.selectNumberOfMonths = function(months){
				try{
					if(months == 3 || months == 6 || months == 9){
						// If selected number of months is one from predefined
						if(months == 3){
							$scope.calculatedRental = $scope.lot.lotItemDetails[0].rentalPriceByNumberOfMonths[0].price * $scope.lot.lotItemDetails[0].quantityAvailable;
						} else if(months == 6){
							$scope.calculatedRental = $scope.lot.lotItemDetails[0].rentalPriceByNumberOfMonths[1].price * $scope.lot.lotItemDetails[0].quantityAvailable;
						} else if(months == 9){
							$scope.calculatedRental = $scope.lot.lotItemDetails[0].rentalPriceByNumberOfMonths[2].price * $scope.lot.lotItemDetails[0].quantityAvailable;
						}
						$scope.selectedNumberOfMonths = parseInt(months);
						$scope.numberOfMonthsSelected = true;
						$scope.changeExtendPeriod(months);
					} else if(months != ''){
						// if value is a custom value
						$scope.selectedNumberOfMonths = parseInt(months);
						$scope.calculatedRental = $scope.lot.rent * parseInt(months);
						$scope.numberOfMonthsSelected = true;
						$scope.changeExtendPeriod('custom');
					} else {
						$scope.selectedExtendPeriod = [];
						$scope.numberOfMonthsSelected = false;
					}
				} catch(e){
					$scope.selectedExtendPeriod = [];
					$scope.numberOfMonthsSelected = false;
				}
			};

			$scope.slickPrev = function(selector) {
				$('.' + selector).slick("slickPrev");
			};

			$scope.slickNext = function(selector) {
				$('.' + selector).slick("slickNext");
			};

			$scope.slickConfigObj = {
				arrows: false,
				slidesToShow: 2,
				slidesToScroll: 1,
				adaptiveHeight: true,
				cssEase: 'ease',
				focusOnSelect:false,
				// autoplay: true,
				responsive: [{
					breakpoint: 767,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						dots:true
					}
				}]
			};

			$scope.slickConfigObj2 = {
				arrows: false,
				slidesToShow: 4,
				slidesToScroll: 1,
				adaptiveHeight: true,
				cssEase: 'ease',
				focusOnSelect:false,
				// autoplay: true,
				responsive: [{
					breakpoint: 1199,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						dots:true
					}
				}]
			};


			$scope.initSliders = function(){
				$timeout(function(){
					$('.images-wrapper .images-big').slick({
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: false,
						fade: true,
						asNavFor: '.images-wrapper .images-thumb'
					});

					$('.images-wrapper .images-thumb').slick({
						slidesToShow: 3,
						slidesToScroll: 1,
						asNavFor: '.images-wrapper .images-big',
						dots: false,
						arrows:true,
						centerMode: true,
						focusOnSelect: true,
						prevArrow: '<button type="button" class="slick-prev pn-gradient custom custom-left">Previous</button>',
						nextArrow: '<button type="button" class="slick-next pn-gradient custom custom-right">Next</button>',
						responsive: [
						{
							breakpoint: 991,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 1
							}
						},
						{
							breakpoint: 767,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1
							}
						}
						]
					});
				},500);
			};

			// new code
			$scope.loadingData = true;

			$scope.getSimilarLots = function(lotId){
				$scope.relatedProductsLoading = true;

				var filters = { "lotType": ["rental"]};
				network.getSimilarLots($scope.user.authToken, lotId, filters)
				.then(function(res){
					console.log(res);
					if(res.data.status && res.data.status.toLowerCase() == 'success'){
						$scope.relatedProducts = res.data.response;
					} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
						swal("Oops...", res.data.message, "error");
					}

					$scope.relatedProductsLoading = false;
					$timeout(function() {
						$('.related-products-carousel').slick($scope.slickConfigObj2);
					}, 20);
				}, function(err){
					if(err.status == 401){
						$scope.logoutUser();
					} else {
						swal("Oops...", "Something went wrong. Please try again.", "error");
						$scope.relatedProductsLoading = false;
					}
				});
			};

			$scope.getLotDetail = function(lotId){
				network.getLotDetail($scope.user.authToken, lotId)
				.then(function(res){
					console.log(res);
					if(res.data.status && res.data.status.toLowerCase() == 'success'){
						$scope.lot = res.data.response;
						$scope.lot.price = $scope.lot.lotItemDetails[0].perItemPrice * $scope.lot.lotItemDetails[0].quantityAvailable;
						$scope.lot.rent = $scope.lot.lotItemDetails[0].rentalPricePerMonth * $scope.lot.lotItemDetails[0].quantityAvailable;
						$scope.initSliders();

						$scope.rentalBookingDetails = res.data.response.lotDetails.rentalBookingDetails;
						$scope.maxMonthsAvailable = $scope.rentalBookingDetails.maxMonthsAvailable;
						$scope.dateOptions.minDate = new Date($scope.rentalBookingDetails.bookingAvailabilityStartDate);
						$scope.dateOptions.maxDate  = new Date($scope.rentalBookingDetails.bookingAvailabilityEndDate);
						$scope.dt = $scope.dateOptions.minDate;

						$scope.getSimilarLots($scope.lot.lotDetails.lotId);
						$scope.findAvailableBuyProduct($scope.lot.lotItemDetails[0].baseProductId, $scope.lot.lotItemDetails[0].productId);

						$scope.loadingData = false;
					} else {
						$state.go('home', {}, {reload: true});
					}
				}, function(err){
					console.log(err);
					if(err.status == 401){
						$scope.logoutUser();
						// swal("Unauthorized", "You are not authorized to perform this action", "error");
					} else {
						swal("Oops...", "Something went wrong. Please try again.", "error");
					}
					$scope.loadingData = false;
				});
			};

			if($stateParams.id && $stateParams.id.trim() != ''){
				$scope.getLotDetail($stateParams.id);
				$scope.lotId = $stateParams.id;
			}

			$scope.findAvailableBuyProduct = function(baseProductId, productId){
				var filters = {
					"baseProductIds": [baseProductId],
					"productIds": [productId],
					"lotType": ["buy"]
				};
				network.doesLotExist(filters)
				.then(function(res){
					if(res.data.status && res.data.status.toLowerCase() == "success" && res.data.foundLot){
						$scope.availableBuyProduct = {
							"lotId": res.data.response[0].lotDetails.lotId,
							"price": res.data.response[0].lotItemDetails[0].perItemPrice * res.data.response[0].lotItemDetails[0].quantityAvailable
						};
					}
				}, function(err){
					console.log(err);
				});
			};

			$scope.checkOut = function(){
				if(authService.isUserLoggedIn()){
					if($scope.numberOfMonthsSelected){
						if($scope.maxMonthsAvailable >= $scope.numberOfMonthsSelected){
							try{
								var prepareData = {
									"lotId": $scope.lot.lotDetails.lotId,
									"monthsToRent": $scope.selectedNumberOfMonths,
									"rent": $scope.calculatedRental,
									"startRentalDate": moment($scope.dt).format("YYYY-MM-DD")
								};
								
								$state.go('cart.rental', prepareData);
							} catch(e){
								swal("Oops...", "Please selecte any of the given options to proceed further.", "error");
								console.log(e);
							}
							console.log(prepareData);
						} else {
							swal('Invalid Selection', "You can rent this product upto "+$scope.maxMonthsAvailable+" only", "warning")
						}
					} else {
						swal("Oops...", "Please select 'Number of months' to proceed further", "error");
					}
				} else {
					swal("Oops...","You need to login to perform this action", "warning");
				}
			};

			$scope.addLotToWishlist = function(lot){
				console.log(lot);
				$scope.addToWishlist(lot);
			};
			$scope.removeLotFromWishlist = function(lot){
				console.log(lot);
				$scope.removeFromWishlist(lot);
			};
		};

	})();