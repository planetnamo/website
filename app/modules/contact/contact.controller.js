(function() {
	'use strict';
	angular
	.module('app')
	.controller('ContactController', ContactController);

	// ContactController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	ContactController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network'];

	// function ContactController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function ContactController($scope, $rootScope, $state, $timeout, network) {
			
			$scope.contactForm = {};
			
			$scope.initForm = function(){
				$scope.contactForm.name = "";
				$scope.contactForm.email = "";
				$scope.contactForm.message = "";
			};
			$scope.initForm();

			$scope.initMap = function(){
				$scope.latLng = {lat: 28.569126, lng: 77.188885};
				$scope.map = new google.maps.Map(document.getElementById('contact-map'), {
					zoom: 14,
					center: $scope.latLng,
					scrollwheel: false,
					navigationControl: false,
					mapTypeControl: false
				});
				$scope.marker = new google.maps.Marker({
					position: $scope.latLng,
					map: $scope.map
				});
			};

			$scope.initMap();

			$scope.isEmailValid = function(email){
				var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return re.test(email);
			};

			$scope.isPhoneValid = function(phone){
				var re = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;
				return re.test(phone);
			};

			$scope.isFormInputValid = function(){
				if($scope.contactForm.name.trim() == ""){
					swal("Oops...", "Please input your name", "error");
					return false;
				} else if($scope.contactForm.email.trim() == "" || !$scope.isEmailValid($scope.contactForm.email.trim())){
					swal("Oops...", "Please input a valid email", "error");
					return false;
				} else if($scope.contactForm.message.trim() == ""){
					swal("Oops...", "Please input a message", "error");
					return false;
				} else {
					return true;
				}
			};

			$scope.sendEmail = function(){
				if($scope.isFormInputValid()){
					// network call to send email
					network.sendEmail($scope.contactForm)
					.then(function(res){
						console.log(res);
						$scope.initForm();
						swal("Sent", "Successfully sent!", "success");	
					}, function(err){
						console.log(err);
					});
				}
			}

		};

	})();