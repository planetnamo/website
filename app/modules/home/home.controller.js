(function() {
	'use strict';
	angular
	.module('app')
	.controller('HomeController', HomeController);

	// HomeController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	HomeController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network'];

	// function HomeController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function HomeController($scope, $rootScope, $state, $timeout, network) {

			// To hide login modal after successfull login
			$('#login-signup-modal').modal('hide');


			// if($scope.$parent.user && $scope.$parent.user.id){
			// 	$scope.id
			// }

			$scope.goToBuy = function(lotType){
				$state.go('buy', {"lotType": lotType});
			};

			$scope.goToBuyWithParams = function(params){
				console.log(params);
				console.log(typeof params);
				var url = params.join(",");
				url = "categories="+url;
				url = "&brands="+url;
				url = "&something="+"abc,xyz";
				url = encodeURIComponent(url);
				$state.go('buyWithQuery', {query:url});
			};

			$scope.products = [
			{
				"id":"1",
				"name":"HP Pavillion 13",
				"price":"16563",
				"rating":"4"
			},
			{
				"id":"2",
				"name":"HP Pavillion 13",
				"price":"21663",
				"rating":"3.5"
			},
			{
				"id":"3",
				"name":"Dell XPS Series",
				"price":"12554",
				"rating":"4"
			},
			{
				"id":"4",
				"name":"HP Pavillion 13",
				"price":"4563",
				"rating":"3"
			},
			{
				"id":"5",
				"name":"Lenovo laptop",
				"price":"16563",
				"rating":"2"
			},
			{
				"id":"6",
				"name":"Dell XPS Series",
				"price":"12554",
				"rating":"4"
			},
			{
				"id":"7",
				"name":"HP Pavillion 13",
				"price":"4563",
				"rating":"3"
			}
			];

			$scope.slickPrev = function(selector) {
				$('.' + selector).slick("slickPrev");
			};

			$scope.slickNext = function(selector) {
				$('.' + selector).slick("slickNext");
			};

			$scope.slickConfigObj = {
				arrows: false,
				slidesToShow: 2,
				slidesToScroll: 1,
				adaptiveHeight: true,
				cssEase: 'ease',
				focusOnSelect:false,
				// autoplay: true,
				responsive: [{
					breakpoint: 767,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						dots:true
					}
				}]
			};

			$scope.slickConfigObj2 = {
				arrows: false,
				slidesToShow: 4,
				slidesToScroll: 1,
				adaptiveHeight: true,
				cssEase: 'ease',
				focusOnSelect:false,
				// autoplay: true,
				responsive: [{
					breakpoint: 1199,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						dots:true
					}
				}]
			};

			$timeout(function() {
				$('.testimonial-carousel').slick($scope.slickConfigObj);
			}, 1000);

			$scope.getHotSellingProducts = function(){
				$scope.hotSellingProductsLoading = true;

				var filters = { "lotType": ["buy"]};
				network.getHotSellingProducts($scope.user.authToken, filters)
				.then(function(res){
					console.log(res);
					if(res.data.status && res.data.status.toLowerCase() == 'success'){
						$scope.hotSellingProducts = res.data.response;
					} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
						swal("Oops...", res.data.message, "error");
					}

					$scope.hotSellingProductsLoading = false;
					$timeout(function() {
						$('.hot-selling-carousel').slick($scope.slickConfigObj2);
					}, 20);
				}, function(err){
					if(err.status == 401){
						$scope.logoutUser();
					} else {
						swal("Oops...", "Something went wrong. Please try again.", "error");
						$scope.hotSellingProductsLoading = false;
					}
				});
			};

			$scope.getNewAddedProducts = function(){
				$scope.newAddedProductsLoading = true;

				var filters = { "lotType": ["buy"]};
				network.getLots($scope.user.authToken, filters)
				.then(function(res){
					console.log(res);
					if(res.data.status && res.data.status.toLowerCase() == 'success'){
						$scope.newAddedProducts = res.data.response;
					} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
						swal("Oops...", res.data.message, "error");
					}

					$scope.newAddedProductsLoading = false;
					$timeout(function() {
						$('.new-added-carousel').slick($scope.slickConfigObj2);
					}, 20);
				}, function(err){
					if(err.status == 401){
						$scope.logoutUser();
					} else {
						swal("Oops...", "Something went wrong. Please try again.", "error");
						$scope.newAddedProductsLoading = false;
					}
				});
			};

			$scope.addLotToWishlist = function(lot){
				console.log(lot);
				$scope.addToWishlist(lot);
			};
			$scope.removeLotFromWishlist = function(lot){
				console.log(lot);
				$scope.removeFromWishlist(lot);
			};

			$scope.getNewAddedProducts();

		};

	})();