(function() {
	'use strict';
	angular
	.module('app')
	.controller('DetailsDirectBuyController', DetailsDirectBuyController);

	// DetailsDirectBuyController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	DetailsDirectBuyController.$inject = ['$scope', '$rootScope', '$state', 'authService', '$stateParams', '$timeout', 'network'];

	// function DetailsDirectBuyController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function DetailsDirectBuyController($scope, $rootScope, $state, authService, $stateParams, $timeout, network) {
			
			$scope.navTabs = [
			{
				"name":"SPECIFICATIONS"	
			},
			{
				"name":"FEATURES"	
			},
			{
				"name":"WARRANTY"	
			},
			{
				"name":"T&Cs"	
			}
			];

			$scope.activeTabs = [];
			$scope.goToTab = function(tab){
				$scope.activeTabs = [];
				$scope.activeTabs[tab] = "active";
			};
			$scope.goToTab($scope.navTabs[0].name);

			$scope.initSliders = function(){
				$timeout(function(){
					$('.images-wrapper .images-big').slick({
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: false,
						fade: true,
						asNavFor: '.images-wrapper .images-thumb'
					});

					$('.images-wrapper .images-thumb').slick({
						slidesToShow: 3,
						slidesToScroll: 1,
						asNavFor: '.images-wrapper .images-big',
						dots: false,
						arrows:true,
						centerMode: true,
						focusOnSelect: true,
						prevArrow: '<button type="button" class="slick-prev pn-gradient custom custom-left">Previous</button>',
						nextArrow: '<button type="button" class="slick-next pn-gradient custom custom-right">Next</button>',
						responsive: [
						{
							breakpoint: 991,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 1
							}
						},
						{
							breakpoint: 767,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1
							}
						}
						]
					});
				},500);
			};

			$scope.loadingData = true;

			$scope.getLotDetail = function(lotId){
				network.getLotDetail($scope.user.authToken, lotId)
				.then(function(res){
					console.log(res);
					if(res.data.status && res.data.status.toLowerCase() == 'success'){
						$scope.lot = res.data.response;
						$scope.lot.price = $scope.lot.lotItemDetails[0].perItemPrice * $scope.lot.lotItemDetails[0].quantityAvailable;
						$scope.lot.lotDetails.dateWindow.startDate = moment($scope.lot.lotDetails.dateWindow.startDate).format("DD MMM, YYYY HH:mm A");
						$scope.lot.startDate = moment($scope.lot.lotDetails.dateWindow.startDate, "DD MMM, YYYY HH:mm A").format("DD MMM, YYYY");
						$scope.lot.startTime = moment($scope.lot.lotDetails.dateWindow.startDate, "DD MMM, YYYY HH:mm A").format("HH:mm A");
						$scope.initSliders();
						$scope.findAvailableRentalProduct($scope.lot.lotItemDetails[0].baseProductId, $scope.lot.lotItemDetails[0].productId);

						$scope.loadingData = false;
					} else {
						$state.go('home', {}, {reload: true});
					}
				}, function(err){
					console.log(err);
					if(err.status == 401){
						$scope.logoutUser();
						// swal("Unauthorized", "You are not authorized to perform this action", "error");
					} else {
						swal("Oops...", "Something went wrong. Please try again.", "error");
					}
					$scope.loadingData = false;
				});
			};

			$scope.findAvailableRentalProduct = function(baseProductId, productId){
				var filters = {
					"baseProductIds": [baseProductId],
					"productIds": [productId],
					"lotType": ["rental"]
				};
				network.doesLotExist(filters)
				.then(function(res){
					if(res.data.status && res.data.status.toLowerCase() == "success" && res.data.foundLot){
						$scope.availableRentalProduct = {
							"lotId": res.data.response[0].lotDetails.lotId,
							"price": res.data.response[0].lotItemDetails[0].rentalPricePerMonth * res.data.response[0].lotItemDetails[0].quantityAvailable
						};
					}
				}, function(err){
					console.log(err);
				});
			};

			$scope.buyNow = function(){
				if(authService.isUserLoggedIn()){
					$state.go('cart.delivery', {"lotId": $scope.lot.lotDetails.lotId});
				} else {
					swal("Oops...","You need to login to perform this action", "warning");
				}
			};

			if($stateParams.id && $stateParams.id.trim() != ''){
				$scope.getLotDetail($stateParams.id);
				$scope.lotId = $stateParams.id;
			}

			$scope.addLotToWishlist = function(lot){
				console.log(lot);
				$scope.addToWishlist(lot);
			};
			$scope.removeLotFromWishlist = function(lot){
				console.log(lot);
				$scope.removeFromWishlist(lot);
			};
		};

	})();