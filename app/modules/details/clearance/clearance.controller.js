(function() {
	'use strict';
	angular
	.module('app')
	.controller('DetailsClearanceController', DetailsClearanceController);

	// DetailsClearanceController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	DetailsClearanceController.$inject = ['$scope', '$rootScope', '$state', 'authService', '$timeout', '$stateParams', 'network'];

	// function DetailsClearanceController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function DetailsClearanceController($scope, $rootScope, $state, authService, $timeout, $stateParams, network) {
			$scope.navTabs = [
			{
				"name":"LOT DETAILS"	
			},
			{
				"name":"SPECIFICATIONS"	
			},
			{
				"name":"FEATURES"	
			},
			{
				"name":"WARRANTY"	
			},
			{
				"name":"T&Cs"	
			}
			];

			$scope.indexController = $scope.$parent.$parent;
			$scope.detailsController = $scope.$parent;

			$scope.paymentConfigAddToWallet = {
				"key": $scope.indexController.razorPayKey,
				"name": "PlanetNamo Wallet",
				"description": "Wallet Recharge",
				"image": "assets/imgs/planet-namo-logo.png",
				"handler": function(response) {
					$scope.razorpayResponseAddToWallet(response);
				},
				"prefill": {
				},
				"theme": {
					"color": "#3bb549"
				}
			};

			$scope.activeTabs = [];
			$scope.goToTab = function(tab){
				$scope.activeTabs = [];
				$scope.activeTabs[tab] = "active";
			};
			$scope.goToTab($scope.navTabs[0].name);

			$scope.initSliders = function(){
				$timeout(function(){
					$('.images-wrapper .images-big').slick({
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: false,
						fade: true,
						asNavFor: '.images-wrapper .images-thumb'
					});

					$('.images-wrapper .images-thumb').slick({
						slidesToShow: 3,
						slidesToScroll: 1,
						asNavFor: '.images-wrapper .images-big',
						dots: false,
						arrows:true,
						centerMode: true,
						focusOnSelect: true,
						prevArrow: '<button type="button" class="slick-prev pn-gradient custom custom-left">Previous</button>',
						nextArrow: '<button type="button" class="slick-next pn-gradient custom custom-right">Next</button>',
						responsive: [
						{
							breakpoint: 991,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 1
							}
						},
						{
							breakpoint: 767,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1
							}
						}
						]
					});
				},500);
			};

			$scope.initFlipClockTimeLeft = function(id, seconds){
				$timeout(function(){
					$('#countdown-time-'+id).FlipClock(seconds,{
						clockFace: 'HourlyCounter',
						countdown: true
					});
				},100);
			};

			$scope.loadingData = true;

			$scope.getLotDetail = function(lotId){
				network.getLotDetail($scope.user.authToken, lotId)
				.then(function(res){
					console.log(res);
					if(res.data.status && res.data.status.toLowerCase() == 'success'){
						$scope.lot = res.data.response;
						$scope.lot.lotDetails.dateWindow.startDate = moment($scope.lot.lotDetails.dateWindow.startDate).format("DD MMM, YYYY HH:mm A");
						$scope.lot.startDate = moment($scope.lot.lotDetails.dateWindow.startDate, "DD MMM, YYYY HH:mm A").format("DD MMM, YYYY");
						$scope.lot.startTime = moment($scope.lot.lotDetails.dateWindow.startDate, "DD MMM, YYYY HH:mm A").format("HH:mm A");
						$scope.initSliders();
						$scope.quantityPercentage = (($scope.lot.lotItemDetails[0].originalQuantity - $scope.lot.lotItemDetails[0].quantityAvailable)/$scope.lot.lotItemDetails[0].quantityAvailable)*100;

						if($scope.lot.lotDetails && $scope.lot.lotDetails.timeLeftInMS){
							if($scope.lot.lotDetails.timeLeftInMS/(60*60*24*1000)>1){
								$scope.lot.daysLeft = parseInt($scope.lot.lotDetails.timeLeftInMS/(60*60*24*1000));
							}else if($scope.lot.lotDetails.timeLeftInMS<0){
								$scope.lot.lotDetails.timeLeftInMS = 0;
							}else{
								$scope.initFlipClockTimeLeft($scope.lot.lotDetails.lotId, parseInt($scope.lot.lotDetails.timeLeftInMS/1000));
							}
						}

						$scope.loadingData = false;
					} else {
						$state.go('home', {}, {reload: true});
					}
				}, function(err){
					console.log(err);
					if(err.status == 401){
						$scope.logoutUser();
						// swal("Unauthorized", "You are not authorized to perform this action", "error");
					} else {
						swal("Oops...", "Something went wrong. Please try again.", "error");
					}
					$scope.loadingData = false;
				});
			};

			$scope.razorpayResponseAddToWallet = function(response){
				if (response.razorpay_payment_id) {
					$scope.addingAmountToWallet = true;
					var apiData = {
						"razorpayPaymentID": response.razorpay_payment_id,
						"amount": parseInt(parseFloat($scope.amountNeededMore) * 100) // Converting to paise
					};
					network.addAmountToWallet($scope.indexController.user.authToken, apiData)
					.then(function(res) {
						// console.log(res);
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							var postData = {};
							postData.lotId = $scope.lotId;
							postData.orderDetails = {};
							postData.orderDetails[$scope.lot.lotItemDetails[0].productId] = $scope.productQuantityNeeded;
							network.payDepositForClearance($scope.indexController.user.authToken, postData)
							.then(function(res){
								console.log(res);
								if(res.data.status && res.data.status.toLowerCase() == 'success'){
									swal("Success", "EMD paid successfully. You can buy the products once the lot is cleared for sale.", "success");
								}else if(res.data.status && res.data.status.toLowerCase() == 'error' && res.data.amountNeededMore){
									swal("Alert", "", "success");
								} else if (res.data && res.data.status && res.data.status == "error") {
									swal("Oops...", res.data.message, "error");
								}else{
									swal("Oops...", "Unable to reach servers. Please Try Again.", "error");
								}
								$scope.paymentLoading = false;
								$scope.amountNeededMore = 0;
							}, function(err){
								console.log(err);
								if(err.status == 401){
									$scope.logoutUser();
									// swal("Unauthorized", "You are not authorized to perform this action", "error");
								} else {
									swal("Oops...", "Something went wrong. Please try again.", "error");
								}
								$scope.paymentLoading = false;
								$scope.amountNeededMore = 0;
							});
						} else if(res.data && res.data.error){
							swal("Oops...", res.data.error, "error");
							$scope.paymentLoading = false;
							$scope.amountNeededMore = 0;
						}else{
							swal("Oops...", "Unable to add amount to your wallet. Please contact our customer care", "error");
							$scope.paymentLoading = false;
							$scope.amountNeededMore = 0;
						}
					}, function(err) {
						swal("Oops...", "Something went wrong while processing your payment.", "error");
						$scope.paymentLoading = false;
						$scope.amountNeededMore = 0;
					});
				} else if (response.error_code) {
					// console.log(response.error_code);
					$scope.amountNeededMore = 0;
					$scope.paymentLoading = false;
					swal("Oops...", "Unable to process your payment request. Please try again.", "error");
				}
			}

			$scope.proceedToPay = function(){
				if(authService.isUserLoggedIn()){
					if(!isNaN($scope.productQuantityNeeded)){
						swal({
						  title: "Are you sure?",
						  text: "EMD of "+($scope.productQuantityNeeded * $scope.lot.lotItemDetails[0].perItemEmd)+" will be freezed from your wallet.",
						  type: "warning",
						  showCancelButton: true,
						  confirmButtonColor: "#5cb85c",
						  confirmButtonText: "Yes, Continue",
						  cancelButtonText: "No",
						  closeOnConfirm: true,
						  closeOnCancel: true
						},
						function(isConfirm){
						  if (isConfirm) {
						    $scope.paymentLoading = true;
							var postData = {};
							postData.lotId = $scope.lotId;
							postData.orderDetails = {};
							postData.orderDetails[$scope.lot.lotItemDetails[0].productId] = $scope.productQuantityNeeded;
							network.payDepositForClearance($scope.indexController.user.authToken, postData)
							.then(function(res){
								// console.log(res);
								if(res.data.status && res.data.status.toLowerCase() == 'success'){
									swal("Success", "EMD paid successfully. You can buy the products once the lot is cleared for sale.", "success");
								}else if(res.data.status && res.data.status == "error" && res.data.amountNeededMore){
									swal({
									  title: "Amount Less",
									  text: "Recharge wallet with "+res.data.amountNeededMore+" more inorder to continue.",
									  type: "warning",
									  showCancelButton: true,
									  confirmButtonColor: "#5cb85c",
									  confirmButtonText: "Yes, Continue",
									  cancelButtonText: "No",
									  closeOnConfirm: true,
									  closeOnCancel: true
									},
									function(isConfirm){
									  if (isConfirm) {
									    $scope.paymentLoading = true;
									    $scope.paymentConfigAddToWallet.amount = parseInt(parseFloat(res.data.amountNeededMore) * 100); //Converting to paise
										$scope.paymentConfigAddToWallet.prefill.name = $scope.indexController.user.name;
										$scope.paymentConfigAddToWallet.prefill.contact = $scope.indexController.user.mobile;
										$scope.paymentConfigAddToWallet.prefill.mobile = $scope.indexController.user.mobile;
										$scope.paymentConfigAddToWallet.prefill.email = $scope.indexController.user.email;
										$scope.instance = new Razorpay($scope.paymentConfigAddToWallet);
										$scope.amountNeededMore = res.data.amountNeededMore;
										$scope.instance.open();
									  } else {
									  }
									});
								} else if (res.data && res.data.status && res.data.status == "error") {
									swal("Oops...", res.data.message, "error");
								}else{
									swal("Oops...", "Unable to reach servers. Please Try Again.", "error");
								}
								$scope.paymentLoading = false;
							}, function(err){
								console.log(err);
								if(err.status == 401){
									$scope.logoutUser();
									// swal("Unauthorized", "You are not authorized to perform this action", "error");
								} else {
									swal("Oops...", "Something went wrong. Please try again.", "error");
								}
								$scope.paymentLoading = false;
							});
						  } else {
						  }
						});
					}else{
						swal("Oops...","Invalid Required Product Quantity", "warning")
					}
				} else{
					swal("Oops...","You need to login to perform this action", "warning");
				}
			}

			if($stateParams.id && $stateParams.id.trim() != ''){
				$scope.getLotDetail($stateParams.id);
				$scope.lotId = $stateParams.id;
			}

			$scope.addLotToWishlist = function(lot){
				console.log(lot);
				$scope.addToWishlist(lot);
			};
			$scope.removeLotFromWishlist = function(lot){
				console.log(lot);
				$scope.removeFromWishlist(lot);
			};
		};

	})();