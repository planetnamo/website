(function() {
	'use strict';
	angular
	.module('app')
	.controller('DetailsAuctionController', DetailsAuctionController);

	// DetailsAuctionController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	DetailsAuctionController.$inject = ['$scope', '$rootScope', '$state', 'authService', '$stateParams', '$timeout', 'network'];

	// function DetailsAuctionController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function DetailsAuctionController($scope, $rootScope, $state, authService, $stateParams, $timeout, network) {
			
			$scope.navTabs = [
			{
				"name":"LOT DETAILS"	
			},
			{
				"name":"SPECIFICATIONS"	
			},
			{
				"name":"FEATURES"	
			},
			{
				"name":"WARRANTY"	
			},
			{
				"name":"T&Cs"	
			}
			];

			$scope.indexController = $scope.$parent.$parent;

			$scope.activeTabs = [];
			$scope.goToTab = function(tab){
				$scope.activeTabs = [];
				$scope.activeTabs[tab] = "active";
			};
			$scope.goToTab($scope.navTabs[0].name);

			$scope.initSliders = function(){
				$timeout(function(){
					$('.images-wrapper .images-big').slick({
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: false,
						fade: true,
						asNavFor: '.images-wrapper .images-thumb'
					});

					$('.images-wrapper .images-thumb').slick({
						slidesToShow: 3,
						slidesToScroll: 1,
						asNavFor: '.images-wrapper .images-big',
						dots: false,
						arrows:true,
						centerMode: true,
						focusOnSelect: true,
						prevArrow: '<button type="button" class="slick-prev pn-gradient custom custom-left">Previous</button>',
						nextArrow: '<button type="button" class="slick-next pn-gradient custom custom-right">Next</button>',
						responsive: [
						{
							breakpoint: 991,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 1
							}
						},
						{
							breakpoint: 767,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1
							}
						}
						]
					});
				},500);
			};

			$scope.initFlipClockTimeLeft = function(id, seconds){
				$timeout(function(){
					$('#countdown-time-'+id).FlipClock(seconds,{
						clockFace: 'HourlyCounter',
						countdown: true
					});
				},100);
			};

			$scope.loadingData = true;

			$scope.paymentConfigAddToWallet = {
				"key": $scope.razorPayKey,
				"name": "PlanetNamo Wallet",
				"description": "Wallet Recharge",
				"image": "assets/imgs/planet-namo-logo.png",
				"handler": function(response) {
					$scope.razorpayResponseAddToWallet(response);
				},
				"prefill": {
				},
				"theme": {
					"color": "#3bb549"
				}
			};

			$scope.razorpayResponseAddToWallet = function(response){
				if (response.razorpay_payment_id) {
					$scope.addingAmountToWallet = true;
					var apiData = {
						"razorpayPaymentID": response.razorpay_payment_id,
						"amount": parseInt(parseFloat($scope.amountNeededMore) * 100) // Converting to paise
					};

					$scope.paymentLoading = true;
					
					network.addAmountToWallet($scope.user.authToken, apiData)
					.then(function(res) {
						// console.log(res);
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							var postData = {};
							postData.lotId = $scope.lot.lotDetails.lotId;
							postData.payFromWallet = true;
							postData.orderDetails = {};
							postData.orderDetails[$scope.lot.lotItemDetails[0].productId] = $scope.lot.lotItemDetails[0].quantityAvailable;

							network.payEMDForAuction($scope.user.authToken, postData)
							.then(function(res){
								console.log(res);
								if(res.data.status && res.data.status.toLowerCase() == 'success'){
									swal("Success", "EMD paid successfully. You can now bid on this auction.", "success");
									$state.reload();
								}else if(res.data.status && res.data.status.toLowerCase() == 'error' && res.data.amountNeededMore){
									swal("Alert", "", "success");
								} else if (res.data && res.data.status && res.data.status == "error") {
									swal("Oops...", res.data.message, "error");
								}else{
									swal("Oops...", "Unable to reach servers. Please Try Again.", "error");
								}
								$scope.paymentLoading = false;
								$scope.amountNeededMore = 0;
							}, function(err){
								console.log(err);
								if(err.status == 401){
									$scope.logoutUser();
									// swal("Unauthorized", "You are not authorized to perform this action", "error");
								} else {
									swal("Oops...", "Something went wrong. Please try again.", "error");
								}
								$scope.paymentLoading = false;
								$scope.amountNeededMore = 0;
							});
						} else if(res.data && res.data.error){
							swal("Oops...", res.data.error, "error");
							$scope.paymentLoading = false;
							$scope.amountNeededMore = 0;
						}else{
							swal("Oops...", "Unable to add amount to your wallet. Please contact our customer care", "error");
							$scope.paymentLoading = false;
							$scope.amountNeededMore = 0;
						}
					}, function(err) {
						swal("Oops...", "Something went wrong while processing your payment.", "error");
						$scope.paymentLoading = false;
						$scope.amountNeededMore = 0;
					});
				} else if (response.error_code) {
					// console.log(response.error_code);
					$scope.amountNeededMore = 0;
					$scope.paymentLoading = false;
					swal("Oops...", "Unable to process your payment request. Please try again.", "error");
				}
			};

			$scope.payEMD = function(){
				if(authService.isUserLoggedIn()){
					swal({
					  title: "Are you sure?",
					  text: "EMD of "+($scope.lot.lotItemDetails[0].quantityAvailable * $scope.lot.lotItemDetails[0].perItemEmd)+" will be freezed from your wallet.",
					  type: "warning",
					  showCancelButton: true,
					  confirmButtonColor: "#5cb85c",
					  confirmButtonText: "Yes, Continue",
					  cancelButtonText: "No",
					  closeOnConfirm: true,
					  closeOnCancel: true
					},
					function(isConfirm){
					  if (isConfirm) {
					    $scope.paymentLoading = true;
						var postData = {};
						postData.lotId = $scope.lot.lotDetails.lotId;
						postData.orderDetails = {};
						postData.orderDetails[$scope.lot.lotItemDetails[0].productId] = $scope.lot.lotItemDetails[0].quantityAvailable;
						network.payEMDForAuction($scope.user.authToken, postData)
						.then(function(res){
							// console.log(res);
							if(res.data.status && res.data.status.toLowerCase() == 'success'){
								swal("Success", "EMD paid successfully. You can now bid on this auction.", "success");
								$state.reload();
							}else if(res.data.status && res.data.status == "error" && res.data.amountNeededMore){
								swal({
								  title: "Amount Less",
								  text: "Recharge wallet with "+res.data.amountNeededMore+" more inorder to continue.",
								  type: "warning",
								  showCancelButton: true,
								  confirmButtonColor: "#5cb85c",
								  confirmButtonText: "Yes, Continue",
								  cancelButtonText: "No",
								  closeOnConfirm: true,
								  closeOnCancel: true
								},
								function(isConfirm){
								  if (isConfirm) {
								    $scope.paymentLoading = true;
								    $scope.paymentConfigAddToWallet.amount = parseInt(parseFloat(res.data.amountNeededMore) * 100); //Converting to paise
									$scope.paymentConfigAddToWallet.prefill.name = $scope.user.name;
									$scope.paymentConfigAddToWallet.prefill.contact = $scope.user.mobile;
									$scope.paymentConfigAddToWallet.prefill.mobile = $scope.user.mobile;
									$scope.paymentConfigAddToWallet.prefill.email = $scope.user.email;
									$scope.instance = new Razorpay($scope.paymentConfigAddToWallet);
									$scope.amountNeededMore = res.data.amountNeededMore;
									$scope.instance.open();
								  } else {
								  }
								});
							} else if (res.data && res.data.status && res.data.status == "error") {
								swal("Oops...", res.data.message, "error");
							}else{
								swal("Oops...", "Unable to reach servers. Please Try Again.", "error");
							}
							$scope.paymentLoading = false;
						}, function(err){
							console.log(err);
							if(err.status == 401){
								$scope.logoutUser();
								// swal("Unauthorized", "You are not authorized to perform this action", "error");
							} else {
								swal("Oops...", "Something went wrong. Please try again.", "error");
							}
							$scope.paymentLoading = false;
						});
					  } else {
					  }
					});
				}else{
					swal("Oops...","You need to login to perform this action", "error");
				}
			};

			$scope.countdown = [];

			$scope.getLotDetail = function(lotId){
				network.getLotDetail($scope.user.authToken, lotId)
				.then(function(res){
					// console.log(res);
					if(res.data.status && res.data.status.toLowerCase() == 'success'){
						var localLotData = angular.copy(res.data.response);
						$scope.lot = angular.copy(res.data.response);
						$scope.lot.userBids = [];
						$scope.lot.HighestLotBids = [];
						if(localLotData && localLotData.userBids){
							for(var key in localLotData.userBids ){
								var localUserBid = localLotData.userBids[key];
								localUserBid.childLotId = key;
								$scope.lot.userBids.push(localUserBid);
							}
						}
						if(localLotData && localLotData.HighestLotBids){
							for(var key in localLotData.HighestLotBids ){
								var localHighestBid = localLotData.HighestLotBids[key];
								localHighestBid.childLotId = key;
								$scope.lot.HighestLotBids.push(localHighestBid);
							}
						}
						$scope.lot.lotDetails.dateWindow.startDate = moment($scope.lot.lotDetails.dateWindow.startDate).format("DD MMM, YYYY HH:mm A");
						$scope.lot.startDate = moment($scope.lot.lotDetails.dateWindow.startDate, "DD MMM, YYYY HH:mm A").format("DD MMM, YYYY");
						$scope.lot.startTime = moment($scope.lot.lotDetails.dateWindow.startDate, "DD MMM, YYYY HH:mm A").format("HH:mm A");
						$scope.initSliders();

						if($scope.lot.HighestLotBids && $scope.lot.HighestLotBids.length > 0 && $scope.lot.HighestLotBids[0].bidAmount){
							$scope.lot.placeBidAmount = $scope.lot.HighestLotBids[0].bidAmount;
						}else if($scope.lot.lotItemDetails && $scope.lot.lotItemDetails.length > 0 && $scope.lot.lotItemDetails[0].auctionMinDeposit){
							$scope.lot.placeBidAmount = $scope.lot.lotItemDetails[0].auctionMinDeposit;
						}else{
							$scope.lot.placeBidAmount = 0;
						}
						if($scope.lot.lotDetails && $scope.lot.lotDetails && $scope.lot.lotDetails.bidIncreaseDetails && $scope.lot.lotDetails.bidIncreaseDetails.type == "percentage" && $scope.lot.lotDetails.bidIncreaseDetails.value){
							$scope.lot.bidIncrementAmount = parseInt(($scope.lot.placeBidAmount * $scope.lot.lotDetails.bidIncreaseDetails.value)/100);
							$scope.lot.placeBidAmount = $scope.lot.placeBidAmount + $scope.lot.bidIncrementAmount;
							$scope.lot.initialPlaceBidAmount = angular.copy($scope.lot.placeBidAmount);
						}else if($scope.lot.lotDetails && $scope.lot.lotDetails.length > 0 && $scope.lot.lotDetails.bidIncreaseDetails && $scope.lot.lotDetails.bidIncreaseDetails.value){
							$scope.lot.placeBidAmount = $scope.lot.placeBidAmount + $scope.lot.lotDetails.bidIncreaseDetails.value;
							$scope.lot.initialPlaceBidAmount = angular.copy($scope.lot.placeBidAmount);
							$scope.lot.bidIncrementAmount = $scope.lot.lotDetails.bidIncreaseDetails.value;
						}


						// init flipclocks and days count if lot type is auction
						
						// var endDate = moment($scope.lot.lotDetails.dateWindow.endDate);
						if($scope.lot.lotDetails && $scope.lot.lotDetails.auctionTimeLeftInMS){
							if($scope.lot.lotDetails.auctionTimeLeftInMS/(60*60*24*1000)>1){
								$scope.lot.daysLeft = parseInt($scope.lot.lotDetails.auctionTimeLeftInMS/(60*60*24*1000));
							}else if($scope.lot.lotDetails.auctionTimeLeftInMS<0){
								$scope.lot.lotDetails.auctionTimeLeftInMS = 0;
							}else{
								$scope.initFlipClockTimeLeft($scope.lot.lotDetails.lotId, parseInt($scope.lot.lotDetails.auctionTimeLeftInMS/1000));
							}
						}
						if($scope.lot.lotDetails.auctionId){
							$scope.indexController.listenToAuctionSocket($scope.lot.lotDetails.auctionId);
						}
						// console.log($scope.lot);
						$scope.loadingData = false;
					} else {
						$state.go('home', {}, {reload: true});
					}
				}, function(err){
					console.log(err);
					if(err.status == 401){
						$scope.logoutUser();
						// swal("Unauthorized", "You are not authorized to perform this action", "error");
					} else {
						swal("Oops...", "Something went wrong. Please try again.", "error");
					}
					$scope.loadingData = false;
				});
			};

			$scope.payEMD1 = function(){
				alert();
			};

			if($stateParams.id && $stateParams.id.trim() != ''){
				$scope.getLotDetail($stateParams.id);
				$scope.lotId = $stateParams.id;
			}

			$scope.addLotToWishlist = function(lot){
				console.log(lot);
				$scope.addToWishlist(lot);
			};
			$scope.removeLotFromWishlist = function(lot){
				console.log(lot);
				$scope.removeFromWishlist(lot);
			};

			$scope.plusClicked = function(){
				$scope.lot.placeBidAmount = $scope.lot.placeBidAmount + $scope.lot.bidIncrementAmount;
			}

			$scope.minusClicked = function(){
				$scope.lot.placeBidAmount = $scope.lot.placeBidAmount - $scope.lot.bidIncrementAmount;
				if($scope.lot.placeBidAmount < $scope.lot.initialPlaceBidAmount){
					$scope.lot.placeBidAmount = angular.copy($scope.lot.initialPlaceBidAmount);
				}
			}


			$scope.submitBid = function(){
				if(!isNaN($scope.lot.placeBidAmount) && $scope.lot.placeBidAmount>=0 && $scope.lot.userBids && $scope.lot.userBids.length>0 && $scope.lot.userBids[0].orderDetails && $scope.lot.userBids[0].orderDetails.length>0){
					swal({
						title: "Are you sure?",
						text: "Do you really want to submit this bid?",
						type: "warning",
						showCancelButton: true,
						confirmButtonText: "Yes, Submit",
						closeOnConfirm: true
					},
					function(){
						var postData = {};
						postData.lotId = $scope.lot.lotDetails.lotId;
						postData.bidAmount = $scope.lot.placeBidAmount;
						postData.orderDetails = {};
						postData.orderDetails[$scope.lot.userBids[0].orderDetails[0].productId] = $scope.lot.userBids[0].orderDetails[0].quantity;
						$scope.lot.loadingBidPlace = true;
						// console.log(postData);
						network.placeBidInAuction($scope.indexController.user.authToken, postData)
							.then(function(res) {
								if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
									swal("Success", "Bid successfully placed.", "success");
								}else if (res.data && res.data.status && res.data.status == "error") {
									if(res.data.message){
										swal("Oops...", res.data.message, "error");
									}else{
										swal("Oops...", "Unable to place bid", "error");
									}
								}else{
									swal("Oops...", "Unable to place bid", "error");
								}
								$scope.lot.loadingBidPlace = false;
							}, function(err) {
								$scope.lot.loadingBidPlace = false;
								if(err.status == 401){
									$scope.logoutUser();
									// swal("Unauthorized", "You are not authorized to perform this action", "error");
								} else {
									swal("Oops...", "Something went wrong. Please try again.", "error");
								}
							});
					});
				}else{
					swal("Error", "Please enter valid bid amount", "error");
				}
			};


			$scope.$on('newBid', function(event, data){
				try{
					var dt = JSON.parse(data);
					// console.log(dt);
					if(dt.auctionId && $scope.lot.lotDetails.auctionId && dt.auctionId == $scope.lot.lotDetails.auctionId &&
						dt.lotId && $scope.lot.lotDetails.lotId && dt.lotId == $scope.lot.lotDetails.lotId){
						if(!$scope.lot.HighestLotBids || $scope.lot.HighestLotBids.length == 0){
							$scope.lot.HighestLotBids = [{}];
						}
						$scope.lot.HighestLotBids[0].bidAmount = dt.bidAmount;
						$scope.lot.HighestLotBids[0].userId = dt.userId;
						$scope.lot.placeBidAmount = dt.bidAmount;
						if($scope.lot.HighestLotBids[0].userId == $scope.indexController.user.id){
							$scope.lot.userBids[0].bidAmount = dt.bidAmount;
						}
						if($scope.lot.lotDetails.bidIncreaseDetails.type == "percentage" && $scope.lot.lotDetails.bidIncreaseDetails.value){
							$scope.lot.bidIncrementAmount = parseInt(($scope.lot.placeBidAmount * $scope.lot.lotDetails.bidIncreaseDetails.value)/100);
							$scope.lot.placeBidAmount = $scope.lot.placeBidAmount + $scope.lot.bidIncrementAmount;
							$scope.lot.initialPlaceBidAmount = angular.copy($scope.lot.placeBidAmount);
						}else if($scope.lot.lotDetails.bidIncreaseDetails.value){
							$scope.lot.placeBidAmount = $scope.lot.placeBidAmount + $scope.lot.lotDetails.bidIncreaseDetails.value;
							$scope.lot.initialPlaceBidAmount = angular.copy($scope.lot.placeBidAmount);
							$scope.lot.bidIncrementAmount = $scope.lot.lotDetails.bidIncreaseDetails.value;
						}
					}
				}catch(err){
					console.log(err);
				}
			});

		};

	})();