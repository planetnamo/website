(function() {
	'use strict';
	angular
	.module('app')
	.controller('MeWishlistController', MeWishlistController);

	// MeWishlistController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	MeWishlistController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network'];

	// function MeWishlistController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function MeWishlistController($scope, $rootScope, $state, $timeout, network) {
			$scope.$parent.activeClass = [];
			$scope.$parent.activeClass['wishlist'] = "active";
			$scope.$parent.activeView = 'Wishlist';

			$scope.getWishlist = function(){
				$scope.loadingData = true;
				network.getWishlist($scope.user.authToken)
				.then(function(res){
					// console.log(res);
					if(res.data.status && res.data.status.toLowerCase() == 'success'){
						$scope.wishlist = res.data.response;
						for(var i=0; i<$scope.wishlist.length; i++){
							if($scope.wishlist[i]){
								if(!$scope.wishlist[i].lotDetails){
									$scope.wishlist[i].lotDetails = {};
								}
								$scope.wishlist[i].lotDetails.isInWishlist = true;
							}
						}
					} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
						swal("Oops...", res.data.message, "error");
					}
					$scope.loadingData = false;
				}, function(err){
					if(err.status == 401){
						$scope.logoutUser();
						// swal("Unauthorized", "You are not authorized to perform this action", "error");
					} else {
						swal("Oops...", "Something went wrong. Please try again.", "error");
					}
					$scope.loadingData = false;
				});
			};

			$scope.getWishlist();

			$scope.addLotToWishlist = function(lot){
				// console.log(lot);
				$scope.addToWishlist(lot);
			};
			$scope.removeLotFromWishlist = function(lot){
				// console.log(lot);
				$scope.removeFromWishlist(lot);
			};

		};

	})();