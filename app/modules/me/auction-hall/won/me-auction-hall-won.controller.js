(function() {
	'use strict';
	angular
	.module('app')
	.controller('MeAuctionHallWonController', MeAuctionHallWonController);

	// MeAuctionHallWonController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	MeAuctionHallWonController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network', 'authService'];

	// function MeAuctionHallWonController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function MeAuctionHallWonController($scope, $rootScope, $state, $timeout, network, authService) {
			$scope.$parent.auctionHallActiveClass = [];
			$scope.$parent.auctionHallActiveClass['won'] = "active";

			$scope.indexController = $scope.$parent.$parent.$parent;
			$scope.meController = $scope.$parent.$parent;
			$scope.auctionController = $scope.$parent;

			$scope.loadMoreWonAuctions = function(){
				$scope.auctionController.loadMoreWonAuctions(function(errorString){
					
				});
			}

			$scope.paymentConfigAddToWallet = {
				"key": $scope.indexController.razorPayKey,
				"name": "PlanetNamo Wallet",
				"description": "Wallet Recharge",
				"image": "assets/imgs/planet-namo-logo.png",
				"handler": function(response) {
					$scope.razorpayResponseAddToWallet(response);
				},
				"prefill": {
				},
				"theme": {
					"color": "#3bb549"
				}
			};

			$scope.razorpayResponseAddToWallet = function(response){
				if (response.razorpay_payment_id) {
					$scope.addingAmountToWallet = true;
					var apiData = {
						"razorpayPaymentID": response.razorpay_payment_id,
						"amount": parseInt(parseFloat($scope.amountNeededMore) * 100) // Converting to paise
					};
					network.addAmountToWallet($scope.indexController.user.authToken, apiData)
					.then(function(res) {
						// console.log(res);
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							var postData = {};
							postData.bidId = $scope.auctionController.wonAuctions[$scope.currentIndex].userBids[0].bidId;
							network.buyWonAuctionOfUser($scope.indexController.user.authToken, postData)
							.then(function(res){
								// console.log(res);
								if(res.data.status && res.data.status.toLowerCase() == 'success'){
									if(!$scope.auctionController.wonAuctions[$scope.currentIndex].userBids[0].currentHighestBid){
										$scope.auctionController.wonAuctions[$scope.currentIndex].userBids[0].currentHighestBid = {};
									}
									$scope.auctionController.wonAuctions[$scope.currentIndex].userBids[0].currentHighestBid.bidWon = true;
									swal("Success", "Congrats on your new product! We'll reach out to you shortly with delivery information.", "success");
								}else if(res.data.status && res.data.status.toLowerCase() == 'error' && res.data.amountNeededMore){
									swal("Alert", "", "success");
								} else if (res.data && res.data.status && res.data.status == "error") {
									swal("Oops...", res.data.message, "error");
								}else{
									swal("Oops...", "Unable to reach servers. Please Try Again.", "error");
								}
								$scope.auctionController.wonAuctions[$scope.currentIndex].buyingOrBackOutLoading = false;
								$scope.amountNeededMore = 0;
							}, function(err){
								console.log(err);
								if(err.status == 401){
									$scope.logoutUser();
									// swal("Unauthorized", "You are not authorized to perform this action", "error");
								} else {
									swal("Oops...", "Something went wrong. Please try again.", "error");
								}
								$scope.auctionController.wonAuctions[$scope.currentIndex].buyingOrBackOutLoading = false;
								$scope.amountNeededMore = 0;
							});
						} else if(res.data && res.data.error){
							swal("Oops...", res.data.error, "error");
							$scope.auctionController.wonAuctions[$scope.currentIndex].buyingOrBackOutLoading = false;
							$scope.amountNeededMore = 0;
						}else{
							swal("Oops...", "Unable to add amount to your wallet. Please contact our customer care", "error");
							$scope.auctionController.wonAuctions[$scope.currentIndex].buyingOrBackOutLoading = false;
							$scope.amountNeededMore = 0;
						}
					}, function(err) {
						swal("Oops...", "Something went wrong while processing your payment.", "error");
						$scope.auctionController.wonAuctions[$scope.currentIndex].buyingOrBackOutLoading = false;
						$scope.amountNeededMore = 0;
					});
				} else if (response.error_code) {
					// console.log(response.error_code);
					$scope.amountNeededMore = 0;
					$scope.auctionController.wonAuctions[$scope.currentIndex].buyingOrBackOutLoading = false;
					swal("Oops...", "Unable to process your payment request. Please try again.", "error");
				}
			}

			$scope.proceedToPay = function(data, index){
				if(authService.isUserLoggedIn()){
					if(!isNaN($scope.auctionController.wonAuctions[index].userBids[0].remainingAmountToPay)){
						swal({
						  title: "Are you sure?",
						  text: "Buy the auction won using the amount from your wallet",
						  type: "warning",
						  showCancelButton: true,
						  confirmButtonColor: "#5cb85c",
						  confirmButtonText: "Yes, Continue",
						  cancelButtonText: "No",
						  closeOnConfirm: true,
						  closeOnCancel: true
						},
						function(isConfirm){
						  if (isConfirm) {
						    $scope.auctionController.wonAuctions[$scope.currentIndex].buyingOrBackOutLoading = true;
							var postData = {};
							postData.bidId = $scope.auctionController.wonAuctions[index].userBids[0].bidId;
							network.buyWonAuctionOfUser($scope.indexController.user.authToken, postData)
							.then(function(res){
								// console.log(res);
								if(res.data.status && res.data.status.toLowerCase() == 'success'){
									if(!$scope.auctionController.wonAuctions[$scope.currentIndex].userBids[0].currentHighestBid){
										$scope.auctionController.wonAuctions[$scope.currentIndex].userBids[0].currentHighestBid = {};
									}
									$scope.auctionController.wonAuctions[$scope.currentIndex].userBids[0].currentHighestBid.bidWon = true;
									swal("Success", "Congrats on your new product! We'll reach out to you shortly with delivery information.", "success");
								}else if(res.data.status && res.data.status == "error" && res.data.amountNeededMore){
									swal({
									  title: "Amount Less",
									  text: "Recharge wallet with "+res.data.amountNeededMore+" more inorder to continue.",
									  type: "warning",
									  showCancelButton: true,
									  confirmButtonColor: "#5cb85c",
									  confirmButtonText: "Yes, Continue",
									  cancelButtonText: "No",
									  closeOnConfirm: true,
									  closeOnCancel: true
									},
									function(isConfirm){
									  if (isConfirm) {
									    $scope.auctionController.wonAuctions[$scope.currentIndex].buyingOrBackOutLoading = true;
									    $scope.paymentConfigAddToWallet.amount = parseInt(parseFloat(res.data.amountNeededMore) * 100); //Converting to paise
										$scope.paymentConfigAddToWallet.prefill.name = $scope.indexController.user.name;
										$scope.paymentConfigAddToWallet.prefill.contact = $scope.indexController.user.mobile;
										$scope.paymentConfigAddToWallet.prefill.mobile = $scope.indexController.user.mobile;
										$scope.paymentConfigAddToWallet.prefill.email = $scope.indexController.user.email;
										$scope.instance = new Razorpay($scope.paymentConfigAddToWallet);
										$scope.amountNeededMore = res.data.amountNeededMore;
										$scope.instance.open();
									  } else {
									  }
									});
								} else if (res.data && res.data.status && res.data.status == "error") {
									swal("Oops...", res.data.message, "error");
								}else{
									swal("Oops...", "Unable to reach servers. Please Try Again.", "error");
								}
								$scope.auctionController.wonAuctions[$scope.currentIndex].buyingOrBackOutLoading = false;
							}, function(err){
								console.log(err);
								if(err.status == 401){
									$scope.logoutUser();
									// swal("Unauthorized", "You are not authorized to perform this action", "error");
								} else {
									swal("Oops...", "Something went wrong. Please try again.", "error");
								}
								$scope.auctionController.wonAuctions[$scope.currentIndex].buyingOrBackOutLoading = false;
							});
						  } else {
						  }
						});
					}else{
						swal("Oops...","Invalid Required Product Quantity", "warning")
					}
				} else{
					swal("Oops...","You need to login to perform this action", "warning");
				}
			}

			$scope.clickedOnPayBalance = function(data,index){
				$scope.currentIndex = index;
				if($scope.auctionController.wonAuctions && $scope.auctionController.wonAuctions.length > index && $scope.auctionController.wonAuctions[index] &&
					$scope.auctionController.wonAuctions[index].userBids && $scope.auctionController.wonAuctions[index].userBids[0] && 
					!isNaN($scope.auctionController.wonAuctions[index].userBids[0].remainingAmountToPay)){
					$scope.proceedToPay(data, index);
				}
			}

			$scope.backOutClicked = function(data, index){
				if($scope.auctionController.wonAuctions && $scope.auctionController.wonAuctions.length > index){
					swal({
					  title: "Are you sure?",
					  text: "EMD Paid will forfeited.",
					  type: "warning",
					  showCancelButton: true,
					  confirmButtonColor: "#3c7b44",
					  confirmButtonText: "Proceed",
					  cancelButtonText: "Cancel",
					  closeOnConfirm: true,
					  closeOnCancel: true
					},
					function(isConfirm){
					  if (isConfirm) {
					    $scope.auctionController.wonAuctions[$scope.currentIndex].buyingOrBackOutLoading = true;
						var postData = {};
						postData.bidId = $scope.auctionController.wonAuctions[index].userBids[0].bidId;
						// console.log(postData);
						// console.log($scope.indexController.user.authToken);
						network.backOutFromWonAuctionOfUser($scope.indexController.user.authToken, postData)
						.then(function(res) {
							console.log(res);
							if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
								$scope.auctionController.wonAuctions[$scope.currentIndex].buyingOrBackOutLoading = false;
								$scope.auctionController.wonAuctions[index].cancelBuyingOrder = true;
								swal("Success", "Backed out successfully.", "success");
							} else if (res.data && res.data.status && res.data.status == "error") {
								swal("Oops...", res.data.message, "error");
								$scope.auctionController.wonAuctions[$scope.currentIndex].buyingOrBackOutLoading = false;
							}else{
								$scope.auctionController.wonAuctions[$scope.currentIndex].buyingOrBackOutLoading = false;
							}
						}, function(err) {
							$scope.auctionController.wonAuctions[$scope.currentIndex].buyingOrBackOutLoading = false;
							if(err.status == 401){
								$scope.logoutUser();
								// swal("Unauthorized", "You are not authorized to perform this action", "error");
							} else {
								swal("Oops...", "Something went wrong. Please try again.", "error");
							}
						});
					  } else {
					    
					  }
					});
				}
			}

		};

	})();