(function() {
	'use strict';
	angular
	.module('app')
	.controller('MeAuctionHallController', MeAuctionHallController);

	// MeAuctionHallController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	MeAuctionHallController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network'];

	// function MeAuctionHallController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function MeAuctionHallController($scope, $rootScope, $state, $timeout, network) {
			$scope.$parent.activeClass = [];
			$scope.$parent.activeClass['auctionHall'] = "active";
			$scope.$parent.activeView = 'Auction Hall';

			$scope.auctionHallActiveClass = [];

			$scope.indexController = $scope.$parent.$parent;
			$scope.meController = $scope.$parent;

			$scope.liveAuctions = [];
			$scope.loadingMoreDataLiveAuctions = false;
			$scope.loadMoreLiveAuctionsFromServer = true;

			$scope.getAllLiveAuctions =function(postData, cb){
				// console.log(postData);
				network.getLiveAuctionsOfUser($scope.indexController.user.authToken, postData)
					.then(function(res) {
						// console.log(res);
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							if(res.data.response && res.data.response && res.data.response.length > 0){
								for(var i=0; i<res.data.response.length; i++){
									var localAuctionsObj = res.data.response[i];
									if(localAuctionsObj.userBids){
										var localUserBids = angular.copy(localAuctionsObj.userBids);
										localAuctionsObj.userBids = [];
										for(var key in localUserBids){
											var localUserBidPush = localUserBids[key];
											localUserBidPush.childLotId = key;
											if(localUserBidPush.userStartedBidding){
												localAuctionsObj.userBids.push(localUserBidPush);
											}
										}
										if(localAuctionsObj.userBids.length > 0){
											$scope.liveAuctions.push(localAuctionsObj);
										}
									}
								}
							}else{
								$scope.loadMoreLiveAuctionsFromServer = false;
							}
							// console.log($scope.liveAuctions);
							cb();
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
							$scope.loadMoreLiveAuctionsFromServer = false;
							cb(res.data.message);
						}else{
							$scope.loadMoreLiveAuctionsFromServer = false;
							cb("No data from server");
						}
					}, function(err) {
						$scope.loadMoreLiveAuctionsFromServer = false;
						if(err.status == 401){
							$scope.logoutUser();
							cb("Authentication Failed");
							// swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
							cb("Something went wrong. Please try again.");
						}
					});
			}

			$scope.loadMoreLiveAuctions = function(cb){
				if($scope.loadMoreLiveAuctionsFromServer && !$scope.loadingMoreDataLiveAuctions){
					$scope.loadingMoreDataLiveAuctions = true;
					var postData = {};
					postData.limit = 30;
					postData.sendLive = true;
					if($scope.liveAuctions && $scope.liveAuctions.length > 0 && $scope.liveAuctions[$scope.liveAuctions.length - 1].createdAt){
						postData.createdOnBefore = $scope.liveAuctions[$scope.liveAuctions.length - 1].createdAt;
					}
					// console.log(postData);
					$scope.getAllLiveAuctions(postData, function(errorString){
						$scope.loadingMoreDataLiveAuctions = false;
						cb(errorString);
					});	
				}else{
					cb();
				}
			}


			$scope.expiredAuctions = [];
			$scope.loadingMoreDataExpiredAuctions = false;
			$scope.loadMoreExpiredAuctionsFromServer = true;

			$scope.getAllExpiredAuctions =function(postData, cb){
				network.getExpiredAuctionsOfUser($scope.indexController.user.authToken, postData)
					.then(function(res) {
						// console.log(res);
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							if(res.data.response && res.data.response && res.data.response.length > 0){
								 for(var i=0; i<res.data.response.length; i++){
									var localAuctionsObj = res.data.response[i];
									if(localAuctionsObj.userBids){
										var localUserBids = angular.copy(localAuctionsObj.userBids);
										localAuctionsObj.userBids = [];
										for(var key in localUserBids){
											var localUserBidPush = localUserBids[key];
											localUserBidPush.childLotId = key;
											localAuctionsObj.userBids.push(localUserBidPush);
										}
										$scope.expiredAuctions.push(localAuctionsObj);
									}
								}
							}else{
								$scope.loadMoreExpiredAuctionsFromServer = false;
							}
							// console.log($scope.expiredAuctions);
							cb();
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
							$scope.loadMoreExpiredAuctionsFromServer = false;
							cb(res.data.message);
						}else{
							$scope.loadMoreExpiredAuctionsFromServer = false;
							cb("No data from server");
						}
					}, function(err) {
						$scope.loadMoreExpiredAuctionsFromServer = false;
						if(err.status == 401){
							$scope.logoutUser();
							cb("Authentication Failed");
							// swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
							cb("Something went wrong. Please try again.");
						}
					});
			}

			$scope.loadMoreExpiredAuctions = function(cb){
				if($scope.loadMoreExpiredAuctionsFromServer && !$scope.loadingMoreDataExpiredAuctions){
					$scope.loadingMoreDataExpiredAuctions = true;
					var postData = {};
					postData.limit = 30;
					postData.sendExpired = true;
					if($scope.expiredAuctions && $scope.expiredAuctions.length > 0 && $scope.expiredAuctions[$scope.expiredAuctions.length - 1].createdAt){
						postData.createdOnBefore = $scope.expiredAuctions[$scope.expiredAuctions.length - 1].createdAt;
					}
					$scope.getAllExpiredAuctions(postData, function(errorString){
						$scope.loadingMoreDataExpiredAuctions = false;
						cb(errorString);
					});	
				}else{
					cb();
				}
			}



			$scope.wonAuctions = [];
			$scope.loadingMoreDataWonAuctions = false;
			$scope.loadMoreWonAuctionsFromServer = true;

			$scope.getAllWonAuctions =function(postData, cb){
				network.getWonAuctionsOfUser($scope.indexController.user.authToken, postData)
					.then(function(res) {
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							if(res.data.response && res.data.response.length > 0){
								 for(var i=0; i<res.data.response.length; i++){
									var localAuctionsObj = res.data.response[i];
									if(localAuctionsObj.userBids){
										var localUserBids = angular.copy(localAuctionsObj.userBids);
										localAuctionsObj.userBids = [];
										for(var key in localUserBids){
											var localUserBidPush = localUserBids[key];
											localUserBidPush.childLotId = key;
											localAuctionsObj.userBids.push(localUserBidPush);
										}
										$scope.wonAuctions.push(localAuctionsObj);
									}
								}
							}else{
								$scope.loadMoreWonAuctionsFromServer = false;
							}
							// console.log($scope.wonAuctions);
							cb();
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
							$scope.loadMoreWonAuctionsFromServer = false;
							cb(res.data.message);
						}else{
							$scope.loadMoreWonAuctionsFromServer = false;
							cb("No data from server");
						}
					}, function(err) {
						$scope.loadMoreWonAuctionsFromServer = false;
						if(err.status == 401){
							$scope.logoutUser();
							cb("Authentication Failed");
							// swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
							cb("Something went wrong. Please try again.");
						}
					});
			}

			$scope.loadMoreWonAuctions = function(cb){
				if($scope.loadMoreWonAuctionsFromServer && !$scope.loadingMoreDataWonAuctions){
					$scope.loadingMoreDataWonAuctions = true;
					var postData = {};
					postData.sendWon = true;
					postData.limit = 30;
					if($scope.wonAuctions && $scope.wonAuctions.length > 0 && $scope.wonAuctions[$scope.wonAuctions.length - 1].createdAt){
						postData.createdOnBefore = $scope.wonAuctions[$scope.wonAuctions.length - 1].createdAt;
					}
					// console.log($scope.indexController.user.authToken);
					// console.log(postData);
					$scope.getAllWonAuctions(postData, function(errorString){
						$scope.loadingMoreDataWonAuctions = false;
						cb(errorString);
					});	
				}else{
					cb();
				}
			}

		};

	})();