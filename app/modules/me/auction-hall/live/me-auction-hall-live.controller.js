(function() {
	'use strict';
	angular
	.module('app')
	.controller('MeAuctionHallLiveController', MeAuctionHallLiveController);

	// MeAuctionHallLiveController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	MeAuctionHallLiveController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network'];

	// function MeAuctionHallLiveController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function MeAuctionHallLiveController($scope, $rootScope, $state, $timeout, network) {
			$scope.$parent.auctionHallActiveClass = [];
			$scope.$parent.auctionHallActiveClass['live'] = "active";

			$scope.indexController = $scope.$parent.$parent.$parent;
			$scope.meController = $scope.$parent.$parent;
			$scope.auctionController = $scope.$parent;

			// console.log($scope.indexController.user);

			$scope.initFlipClockTimeLeft = function(id, seconds){
				$timeout(function(){
					// console.log("called: "+ id + " and "+ seconds);
					$('#countdown-time-'+id).FlipClock(seconds,{
						clockFace: 'HourlyCounter',
						countdown: true
					});
				},100);
			};

			$scope.refreshFlipClocks= function(){
				$timeout(function(){
					angular.forEach($scope.auctionController.liveAuctions, function(val, index){
						if(val.auctionTimeLeftInMS){
							if(val.auctionTimeLeftInMS/(60*60*24*1000)>1){
								$scope.auctionController.liveAuctions[index].daysLeft = parseInt(val.auctionTimeLeftInMS/(60*60*24*1000));
							}else if(val.auctionTimeLeftInMS<0){
								$scope.auctionController.liveAuctions[index].auctionTimeLeftInMS = 0;
							}else{
								$scope.initFlipClockTimeLeft(index, parseInt(val.auctionTimeLeftInMS/1000));
							}
						}
					});
				},1000);
			}

			$scope.plusClicked = function(data, index){
				if($scope.auctionController.liveAuctions && $scope.auctionController.liveAuctions.length > index){
					$scope.auctionController.liveAuctions[index].placeBidAmount = $scope.auctionController.liveAuctions[index].placeBidAmount + $scope.auctionController.liveAuctions[index].bidIncrementAmount;
				}
			}

			$scope.minusClicked = function(data, index){
				if($scope.auctionController.liveAuctions && $scope.auctionController.liveAuctions.length > index){
					$scope.auctionController.liveAuctions[index].placeBidAmount = $scope.auctionController.liveAuctions[index].placeBidAmount - $scope.auctionController.liveAuctions[index].bidIncrementAmount;
					if($scope.auctionController.liveAuctions[index].placeBidAmount < $scope.auctionController.liveAuctions[index].initialPlaceBidAmount){
						$scope.auctionController.liveAuctions[index].placeBidAmount = angular.copy($scope.auctionController.liveAuctions[index].initialPlaceBidAmount);
					}
				}
			}

			$scope.submitBid = function(data, index){
				if(!isNaN(data.placeBidAmount) && data.placeBidAmount>=0 && data.userBids && data.userBids.length>0 && data.userBids[0].orderDetails && data.userBids[0].orderDetails.length>0){
					swal({
						title: "Are you sure?",
						text: "Do you really want to submit this bid?",
						type: "warning",
						showCancelButton: true,
						confirmButtonText: "Yes, Submit",
						closeOnConfirm: true
					},
					function(){
						var postData = {};
						postData.lotId = data.lotId;
						postData.bidAmount = data.placeBidAmount;
						postData.orderDetails = {};
						postData.orderDetails[data.userBids[0].orderDetails[0].productId] = data.userBids[0].orderDetails[0].quantity;
						if($scope.auctionController.liveAuctions && $scope.auctionController.liveAuctions.length > index){
							$scope.auctionController.liveAuctions[index].loadingBidPlace = true;
						}
						// console.log(postData);
						network.placeBidInAuction($scope.indexController.user.authToken, postData)
							.then(function(res) {
								if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
									swal("Success", "Bid successfully placed.", "success");
								}else if (res.data && res.data.status && res.data.status == "error") {
									if(res.data.message){
										swal("Oops...", res.data.message, "error");
									}else{
										swal("Oops...", "Unable to place bid", "error");
									}
								}else{
									swal("Oops...", "Unable to place bid", "error");
								}
								if($scope.auctionController.liveAuctions && $scope.auctionController.liveAuctions.length > index){
									$scope.auctionController.liveAuctions[index].loadingBidPlace = false;
								}
							}, function(err) {
								if($scope.auctionController.liveAuctions && $scope.auctionController.liveAuctions.length > index){
									$scope.auctionController.liveAuctions[index].loadingBidPlace = false;
								}
								if(err.status == 401){
									$scope.logoutUser();
									// swal("Unauthorized", "You are not authorized to perform this action", "error");
								} else {
									swal("Oops...", "Something went wrong. Please try again.", "error");
								}
							});
					});
				}else if(!data.orderInfo || data.orderInfo.length <=0){
					swal("Error", "Please enter valid amount", "error");
				}else{
					swal("Error", "No Order Info Found", "error");
				}
			};

			$scope.refreshSocketListeners = function(){
				$timeout(function(){
					angular.forEach($scope.auctionController.liveAuctions, function(val, index){
						if(val.auctionId){
							$scope.indexController.listenToAuctionSocket(val.auctionId);
						}
					});
				},1000);
			}

			$scope.updateAllAuctionsDataAsNeeded = function(){
				$timeout(function(){
					try{
						for(var i=0; i<$scope.auctionController.liveAuctions.length; i++){
							var localData = $scope.auctionController.liveAuctions[i];
							if(localData.userBids && localData.userBids.length > 0 && localData.userBids[0].currentHighestBid && localData.userBids[0].currentHighestBid.bidAmount){
								$scope.auctionController.liveAuctions[i].placeBidAmount = localData.userBids[0].currentHighestBid.bidAmount;
							}else if(localData.userBids && localData.userBids.length > 0 && localData.userBids[0].orderDetails && localData.userBids[0].orderDetails.length > 0 && localData.userBids[0].orderDetails[0].productStartingBidPrice){
								$scope.auctionController.liveAuctions[i].placeBidAmount = localData.userBids[0].orderInfo[0].productStartingBidPrice;
							}else{
								$scope.auctionController.liveAuctions[i].placeBidAmount = 0;
							}
							if(localData.bidIncreaseDetails.type == "percentage" && localData.bidIncreaseDetails.value){
								$scope.auctionController.liveAuctions[i].placeBidAmount = $scope.auctionController.liveAuctions[i].placeBidAmount + parseInt(($scope.auctionController.liveAuctions[i].placeBidAmount * localData.bidIncreaseDetails.value)/100);
								$scope.auctionController.liveAuctions[i].initialPlaceBidAmount = angular.copy($scope.auctionController.liveAuctions[i].placeBidAmount);
								$scope.auctionController.liveAuctions[i].bidIncrementAmount = parseInt(($scope.auctionController.liveAuctions[i].placeBidAmount * localData.bidIncreaseDetails.value)/100);
							}else if(localData.bidIncreaseDetails.value){
								$scope.auctionController.liveAuctions[i].placeBidAmount = $scope.auctionController.liveAuctions[i].placeBidAmount + localData.bidIncreaseDetails.value;
								$scope.auctionController.liveAuctions[i].initialPlaceBidAmount = angular.copy($scope.auctionController.liveAuctions[i].placeBidAmount);
								$scope.auctionController.liveAuctions[i].bidIncrementAmount = localData.bidIncreaseDetails.value;
							}
						}	
					}catch(err){

					}
				},1000);
			}

			$scope.loadMoreLiveAuctions = function(){
				$scope.auctionController.loadMoreLiveAuctions(function(errorString){
					if(!errorString){
						$scope.updateAllAuctionsDataAsNeeded();
						$scope.refreshFlipClocks();
						$scope.refreshSocketListeners();
					}
				});
			}

			$scope.$on('newBid', function(event, data){
				try{
					var dt = JSON.parse(data);
					// console.log(dt);
					if($scope.auctionController.liveAuctions){
						for(var i=0; i<$scope.auctionController.liveAuctions.length; i++){
							var localAuction = $scope.auctionController.liveAuctions[i];
							if(dt.auctionId && localAuction.auctionId && dt.auctionId == localAuction.auctionId &&
								dt.lotId && localAuction.lotId && dt.lotId == localAuction.lotId){
								if(!localAuction.userBids){
									$scope.auctionController.liveAuctions[i].userBids = [{"currentHighestBid":{}}];
								}
								$scope.auctionController.liveAuctions[i].userBids[0].currentHighestBid.bidAmount = dt.bidAmount;
								$scope.auctionController.liveAuctions[i].userBids[0].currentHighestBid.userId = dt.userId;
								$scope.auctionController.liveAuctions[i].placeBidAmount = dt.bidAmount;
								if($scope.auctionController.liveAuctions[i].userBids[0].currentHighestBid.userId == $scope.indexController.user.id){
									$scope.auctionController.liveAuctions[i].userBids[0].currentUserBidAmount = dt.bidAmount;
								}
								if(localAuction.bidIncreaseDetails.type == "percentage" && localAuction.bidIncreaseDetails.value){
									$scope.auctionController.liveAuctions[i].placeBidAmount = $scope.auctionController.liveAuctions[i].placeBidAmount + parseInt(($scope.auctionController.liveAuctions[i].placeBidAmount * localAuction.bidIncreaseDetails.value)/100);
									$scope.auctionController.liveAuctions[i].initialPlaceBidAmount = angular.copy($scope.auctionController.liveAuctions[i].placeBidAmount);
									$scope.auctionController.liveAuctions[i].bidIncrementAmount = parseInt(($scope.auctionController.liveAuctions[i].placeBidAmount * localAuction.bidIncreaseDetails.value)/100);
								}else if(localAuction.bidIncreaseDetails.value){
									$scope.auctionController.liveAuctions[i].placeBidAmount = $scope.auctionController.liveAuctions[i].placeBidAmount + localAuction.bidIncreaseDetails.value;
									$scope.auctionController.liveAuctions[i].initialPlaceBidAmount = angular.copy($scope.auctionController.liveAuctions[i].placeBidAmount);
									$scope.auctionController.liveAuctions[i].bidIncrementAmount = localAuction.bidIncreaseDetails.value;
								}
								break;
							}
						}
					}
				}catch(err){
					console.log(err);
				}
			});

		};
	})();