(function() {
	'use strict';
	angular
	.module('app')
	.controller('MeSaleController', MeSaleController);

	// MeSaleController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	MeSaleController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network'];

	// function MeSaleController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function MeSaleController($scope, $rootScope, $state, $timeout, network) {
			$scope.$parent.activeClass = [];
			$scope.$parent.activeClass['sales'] = "active";
			$scope.$parent.activeView = 'My Sales';

			$scope.indexController = $scope.$parent.$parent;
			$scope.meController = $scope.$parent;

			$scope.sales = [];

			$scope.getAllSales = function(postData, cb){
				// console.log(postData);
				network.getSalesOfUser($scope.indexController.user.authToken, postData)
					.then(function(res) {
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							if(res.data.response && res.data.response.length > 0){
								$scope.sales = $scope.sales.concat(res.data.response);
							}else{
								$scope.loadMoreDataFromServer = false;
							}
							// console.log($scope.sales);
							cb();
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
							$scope.loadMoreDataFromServer = false;
							cb();
						}else{
							$scope.loadMoreDataFromServer = false;
							cb();
						}
					}, function(err) {
						$scope.loadMoreDataFromServer = false;
						if(err.status == 401){
							$scope.logoutUser();
							cb();
							// swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
							cb();
						}
					});
			}


			$scope.reloadAllPageData = function(){
				$scope.loadingData = true;
				var postData = {};
				postData.limit = 30;
				$scope.sales = [];
				$scope.getAllSales(postData, function(){
					$scope.loadingData = false;
				});	
			}

			// $scope.reloadAllPageData();

			$scope.loadMoreDataFromServer = true;

			$scope.loadMoreSalesData = function(){
				if($scope.loadMoreDataFromServer && !$scope.loadingMoreData){
					$scope.loadingMoreData = true;
					var postData = {};
					postData.limit = 30;
					if($scope.sales && $scope.sales.length > 0 && $scope.sales[$scope.sales.length - 1].SellingOrderInfo && $scope.sales[$scope.sales.length - 1].SellingOrderInfo.createdAt){
						postData.createdOnBefore = $scope.sales[$scope.sales.length - 1].SellingOrderInfo.createdAt;
					}
					$scope.getAllSales(postData, function(){
						$scope.loadingMoreData = false;
					});	
				}
			}

			$scope.getFormattedDate = function(dateString){
				if(dateString){
					try{
						return moment(dateString).format("LLLL");
					}catch(err){

					}
				}
				return "";
			}

			$scope.clickedOnNotInteresetedToSell = function(data, position){
				if(data.SellingOrderInfo.sellingOrderId){
					if($scope.sales && $scope.sales.length > position){
						$scope.sales[position].showLoader = true;
					}
					network.cancelSellingOrderByUser($scope.indexController.user.authToken, data.SellingOrderInfo.sellingOrderId)
					.then(function(res) {
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							swal("Success", "Order have been cancelled", "success");
							if($scope.sales && $scope.sales.length > position && $scope.sales[position].SellingOrderInfo){
								$scope.sales[position].SellingOrderInfo.cancelSellingOrder = true;
							}
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
						}else{
							swal("Oops...", "Unable to contact servers. Please try again", "error");
						}
							if($scope.sales && $scope.sales.length > position){
								$scope.sales[position].showLoader = false;
							}
					}, function(err) {
						$scope.loadMoreDataFromServer = false;
						if(err.status == 401){
							$scope.logoutUser();
							// swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
							if($scope.sales && $scope.sales.length > position){
								$scope.sales[position].showLoader = false;
							}
					});
				}
			}

			$scope.clickedOnWillingToSell = function(data, position){
				if(data.SellingOrderInfo.sellingOrderId){
					if($scope.sales && $scope.sales.length > position){
						$scope.sales[position].showLoader = true;
					}
					network.acceptSellingOrderByUser($scope.indexController.user.authToken, data.SellingOrderInfo.sellingOrderId)
					.then(function(res) {
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							swal("Success", "Approved successfully for selling.", "success");
							if($scope.sales && $scope.sales.length > position && $scope.sales[position].SellingOrderInfo){
								$scope.sales[position].SellingOrderInfo.userApproved = true;
							}
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
						}else{
							swal("Oops...", "Unable to contact servers. Please try again", "error");
						}
							if($scope.sales && $scope.sales.length > position){
								$scope.sales[position].showLoader = false;
							}
					}, function(err) {
						$scope.loadMoreDataFromServer = false;
						if(err.status == 401){
							$scope.logoutUser();
							// swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
							if($scope.sales && $scope.sales.length > position){
								$scope.sales[position].showLoader = false;
							}
					});
				}
			}

			$scope.clickedOnChangeDate = function(data, position){
				if(data.SellingOrderInfo.sellingOrderId){

					$scope.dateOptions = {
						formatYear: 'yy',
						showWeeks: false,
						maxDate: new Date(2020, 1, 1),
						minDate: new Date(),
						startingDay: 1
					};

					if($scope.sales && $scope.sales.length > position){
						$scope.sales[position].datePickerOpened = true;
					}
				}
			}

			$scope.dateChanged = function(data, position){
				$scope.performNetworkForChangeDate(data, position, moment(data.sellingDateSelected).format("YYYY-MM-DD"), moment(data.sellingDateSelected).format("YYYY-MM-DDTHH:mm:ssZ"));
			}

			$scope.performNetworkForChangeDate = function(data, position, newDateString, dateStringInUnivFormat){
				if(data.SellingOrderInfo.sellingOrderId){
					if($scope.sales && $scope.sales.length > position){
						$scope.sales[position].showLoader = true;
					}
					var postData = {};
					postData.sellingOrderId = data.SellingOrderInfo.sellingOrderId;
					postData.newPickUpDate = newDateString;
					// console.log(postData);
					network.requestChangeDateOfSellOrder($scope.indexController.user.authToken, postData)
					.then(function(res) {
						if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
							swal("Success", "Date changed successfully.", "success");
							// console.log(res);
							if($scope.sales && $scope.sales.length > position && $scope.sales[position].deliveryInfo){
								$scope.sales[position].deliveryInfo.schedulePickUpDate = dateStringInUnivFormat;
							}
						} else if (res.data && res.data.status && res.data.status == "error") {
							swal("Oops...", res.data.message, "error");
						}else{
							swal("Oops...", "Unable to contact servers. Please try again", "error");
						}
							if($scope.sales && $scope.sales.length > position){
								$scope.sales[position].showLoader = false;
							}
					}, function(err) {
						$scope.loadMoreDataFromServer = false;
						if(err.status == 401){
							$scope.logoutUser();
							// swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
							if($scope.sales && $scope.sales.length > position){
								$scope.sales[position].showLoader = false;
							}
					});
				}
			}

		};

	})();