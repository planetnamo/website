(function() {
	'use strict';
	angular
	.module('app')
	.controller('CompareBuyController', CompareBuyController);

	// CompareBuyController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	CompareBuyController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network'];

	// function CompareBuyController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function CompareBuyController($scope, $rootScope, $state, $timeout, network) {
			$scope.$parent.activeLotType = [];
			$scope.$parent.activeLotType['buy'] = 'pn-gradient';

			// $scope.compareLots.buy = ["58d65b4e4b04583c308b12c6", "58d657406a52a236f935ccd1"];
			if($scope.compareLots.buy.length > 1){
				$scope.loadingData = true;
				$scope.compareBuyLots = function(){
					network.compareLots($scope.compareLots.buy)
					.then(function(res){
						console.log(res);
						if (res.data.status && res.data.status.toLowerCase() == "success") {
							$scope.buyLots = res.data.response;
						} else if(res.data.status && res.data.status.toLowerCase() == "error") {
							swal("Oops...", res.data.message, "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
						$scope.loadingData = false;
					}, function(err){
						if(err.status == 401){
							$scope.logoutUser();
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
						$scope.loadingData = false;
					});
				}();
			}
		};

	})();