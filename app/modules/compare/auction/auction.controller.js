(function() {
	'use strict';
	angular
	.module('app')
	.controller('CompareAuctionController', CompareAuctionController);

	// CompareAuctionController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	CompareAuctionController.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network'];

	// function CompareAuctionController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function CompareAuctionController($scope, $rootScope, $state, $timeout, network) {
			$scope.$parent.activeLotType = [];
			$scope.$parent.activeLotType['auction'] = 'pn-gradient';
			
			// $scope.compareLots.auction = ["58ee1f2509a17a552bc5c208", "58dcb255cb4d156b1d66b302"];
			if($scope.compareLots.auction.length > 1){
				$scope.loadingData = true;
				$scope.compareAuctionLots = function(){
					network.compareLots($scope.compareLots.auction)
					.then(function(res){
						console.log(res);
						if (res.data.status && res.data.status.toLowerCase() == "success") {
							$scope.auctionLots = res.data.response;
						} else if(res.data.status && res.data.status.toLowerCase() == "error") {
							swal("Oops...", res.data.message, "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
						$scope.loadingData = false;
					}, function(err){
						if(err.status == 401){
							$scope.logoutUser();
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
						$scope.loadingData = false;
					});
				}();
			}
		};

	})();