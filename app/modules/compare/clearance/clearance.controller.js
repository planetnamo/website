(function() {
	'use strict';
	angular
	.module('app')
	.controller('CompareClearanceController', CompareClearanceController);

	// CompareClearanceController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	CompareClearanceController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$timeout', 'network'];

	// function CompareClearanceController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function CompareClearanceController($scope, $rootScope, $state, $stateParams, $timeout, network) {
			$scope.$parent.activeLotType = [];
			$scope.$parent.activeLotType['clearance'] = 'pn-gradient';

			if($scope.compareLots.clearance.length > 1){
				$scope.loadingData = true;
				$scope.compareClearanceLots = function(){
					network.compareLots($scope.compareLots.clearance)
					.then(function(res){
						console.log(res);
						if (res.data.status && res.data.status.toLowerCase() == "success") {
							$scope.clearanceLots = res.data.response;
						} else if(res.data.status && res.data.status.toLowerCase() == "error") {
							swal("Oops...", res.data.message, "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
						$scope.loadingData = false;
					}, function(err){
						if(err.status == 401){
							$scope.logoutUser();
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
						$scope.loadingData = false;
					});
				}();
			}

		};

	})();