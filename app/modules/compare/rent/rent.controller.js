(function() {
	'use strict';
	angular
	.module('app')
	.controller('CompareRentController', CompareRentController);

	// CompareRentController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	CompareRentController.$inject = ['$scope', '$rootScope', '$state', '$timeout'];

	// function CompareRentController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function CompareRentController($scope, $rootScope, $state, $timeout) {
			$scope.$parent.activeLotType = [];
			$scope.$parent.activeLotType['rent'] = 'pn-gradient';

			if($scope.compareLots.rental.length > 1){
				$scope.loadingData = true;
				$scope.comparerentalLots = function(){
					network.compareLots($scope.compareLots.rental)
					.then(function(res){
						console.log(res);
						if (res.data.status && res.data.status.toLowerCase() == "success") {
							$scope.rentalLots = res.data.response;
						} else if(res.data.status && res.data.status.toLowerCase() == "error") {
							swal("Oops...", res.data.message, "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
						$scope.loadingData = false;
					}, function(err){
						if(err.status == 401){
							$scope.logoutUser();
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
						$scope.loadingData = false;
					});
				}();
			}
		};

	})();