(function() {
	'use strict';
	angular
	.module('app')
	.controller('ForgotPasswordController', ForgotPasswordController);

	// ForgotPasswordController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	ForgotPasswordController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$timeout', 'network'];

	// function ForgotPasswordController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function ForgotPasswordController($scope, $rootScope, $state, $stateParams, $timeout, network) {
			
			$scope.resetPassword = function(){
				$scope.email = $stateParams.email;
				$scope.token = $stateParams.updateToken;

				if($scope.password === $scope.retypePassword){
					$scope.showLoading = true;
					var prepareData = {
						"password": $scope.password
					};
					network.resetPassword($scope.email, $scope.token, prepareData)
					.then(function(res){
						if(res.data && res.data.status && res.data.status.toLowerCase() == "success"){
							swal({
								title: "Done!",
								text: "Your password has been updated.",
								type: "success",
								showCancelButton: false,
								confirmButtonColor: "#5cb85c",
								confirmButtonText: "Login",
								closeOnConfirm: true
							},
							function(){
								$scope.password = "";
								$scope.retypePassword = "";
								$state.go("home", {}, {reload: true});
							});
						} else if(res.data && res.data.error){
							swal("Oops...", res.data.error, "error");
						}
						$scope.showLoading = false;
					}, function(err){
						console.log(err);
						$scope.showLoading = false;
					});
				} else {
					swal("Oops...", "Retype password doesn't match. Please type again.", "warning");
				}

			};

		};

	})();