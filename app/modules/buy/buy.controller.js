(function() {
	'use strict';
	angular
	.module('app')
	.controller('BuyController', BuyController);

	// BuyController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	BuyController.$inject = ['$scope', '$rootScope', '$state', '$timeout', '$stateParams', 'network'];

	// function BuyController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function BuyController($scope, $rootScope, $state, $timeout, $stateParams, network) {
			
			// Filters

			$scope.currentLotType = null;

			$scope.filters = {};
			$scope.filters.products = [];
			$scope.filters.brands = [];
			$scope.filters.priceRange = [];
			
			$scope.filters.products = ["Mobile Phones","Desktops", "TV", "Tablets", "I MAC", "Laptops", "Gaming Console", "Home Appliances"];
			$scope.filters.brands = ["Apple","Assus","Dell","Sony","Samsung","HP","Lenovo", "LG"];

			$scope.filters.priceRange = [
			{
				"displayText": "Under Rs. 10,000",
				"minPrice": 0,
				"maxPrice": 10000
			},
			{
				"displayText": "Rs. 10,001 - 30,000",
				"minPrice": 10001,
				"maxPrice": 30000
			},
			{
				"displayText": "Rs. 30,001 - 50,000",
				"minPrice": 30001,
				"maxPrice": 50000
			},
			{
				"displayText": "Rs. 50,001 - 70,000",
				"minPrice": 50001,
				"maxPrice": 70000
			},
			{
				"displayText": "Above Rs. 70,000",
				"minPrice": 70000,
				"maxPrice": 0
			}
			];

			$scope.passFilters = {};

			$scope.selectedPriceRange = {};
			$scope.selectedPriceRange.text = "Select Price Range";

			$scope.changePriceRange = function(range){
				$scope.selectedPriceRange.text = range.displayText;
				$scope.selectedPriceRange.minPrice = range.minPrice;
				$scope.selectedPriceRange.maxPrice = range.maxPrice;
			};

			$scope.initFlipClockTimeLeft = function(id, seconds){
				$timeout(function(){
					$('#countdown-time-'+id).FlipClock(seconds,{
						clockFace: 'HourlyCounter',
						countdown: true
					});
				},100);
			};

			$scope.countdown = [];

			$scope.getLots = function(){
				$scope.loadingLots = true;
				network.getLots($scope.user.authToken, $scope.passFilters)
				.then(function(res){
					console.log(res);
					// $scope.lots = [];
					// $scope.lots = angular.copy(res.data.response);
					$scope.lots = res.data.response;

					// init flipclocks and days count if lot type is auction
					if($scope.currentLotType == 'auction'){
						angular.forEach($scope.lots, function(value, key){
							value.price = value.lotItemDetails[0].perItemPrice * value.lotItemDetails[0].quantityAvailable;
							var endDate = moment(value.lotDetails.dateWindow.endDate);
							if(moment().diff(endDate, "days") > -1 && moment().diff(endDate, "seconds") < -0){
								$scope.initFlipClockTimeLeft(value.lotDetails.lotId, Math.abs(moment().diff(endDate, "seconds")));
							} else if(moment().diff(endDate, "days") < -0){
								var days = Math.abs(moment().diff(endDate, "days"));
								if(days == 1){
									$scope.countdown[value.lotDetails.lotId] = "" + Math.abs(moment().diff(endDate, "days")) + " Day left";
								} else {
									$scope.countdown[value.lotDetails.lotId] = "" + Math.abs(moment().diff(endDate, "days")) + " Days left";
								}
							} else {
							}
						});
						console.log($scope.lots);
					} else {
						angular.forEach($scope.lots, function(value, key){
							if(value.lotType == "clearance"){
								value.quantityPercentage = ((value.lotItemDetails[0].originalQuantity - value.lotItemDetails[0].quantityAvailable)/value.lotItemDetails[0].quantityAvailable)*100;
							}
							value.price = value.lotItemDetails[0].perItemPrice * value.lotItemDetails[0].quantityAvailable;
						});
					}
					
					$scope.numberOfLots = res.data.numberOfLots;
					$scope.loadingLots = false;
				}, function(err){
					$scope.loadingLots = false;
				});
			};

			$scope.activeLotType = [];
			$scope.changeLotType = function(lotType){
				$scope.activeLotType = [];
				$scope.activeLotType[lotType] = 'pn-gradient';
				$scope.passFilters.lotType = lotType;
				$scope.currentLotType = lotType;
				$scope.getLots();
			};

			if($stateParams.lotType && $stateParams.lotType == 'buy' || $stateParams.lotType == 'auction' || $stateParams.lotType == 'clearance'){
				$scope.changeLotType($stateParams.lotType);
			} else {
				$scope.changeLotType('buy');
			}

			// Check if any query param is passed in the url
			// if($stateParams.query && $stateParams.query != ''){
			// 	var queryParam = decodeURIComponent($stateParams.query);
			// 	console.log(queryParam);
			// 	if (queryParam.indexOf('&') > -1){
			// 		var firstArray = queryParam.split("&");
			// 		angular.forEach(firstArray, function(value, key){
			// 			console.log(value);
			// 		});
			// 		// var secondArray = firstArray[0].split("=");
			// 		// if(secondArray[0] && secondArray[0] == "categories"){
			// 		// 	console.log("categories", secondArray[1].split(","));
			// 		// }
			// 	} else {
			// 		var firstArray = queryParam.split("=");
			// 		if(firstArray[0] && firstArray[0] == "categories"){
			// 			console.log("categories", firstArray[1].split(","));
			// 		}
			// 	}
			// }

			$scope.clearAllFilters = function(){
				$scope.appliedFilters.products = [];
				$scope.appliedFilters.brands = [];
				$scope.selectedPriceRange = {};
				$scope.selectedPriceRange.text = "Select Price Range";

				$scope.parsedFilters.products = [];
				$scope.parsedFilters.brands = [];

				$scope.passFilters = {};

				$scope.searchQueryParam = "";
				$scope.passFilters.queryParam = "";

				$scope.changeLotType($scope.currentLotType);
			};

			$scope.appliedFilters = {};
			$scope.appliedFilters.products = [];
			$scope.appliedFilters.brands = {};
			$scope.appliedFilters.priceRange = {};

			$scope.parsedFilters = {};
			$scope.parsedFilters.products = [];
			$scope.parsedFilters.brands = [];

			$scope.applyFilters = function(){
				// parsing products filters
				$scope.parsedFilters.products = [];
				angular.forEach($scope.appliedFilters.products, function(value, key){
					if(value == true){
						$scope.parsedFilters.products.push($scope.filters.products[key]);
					}
				});

				// parsing brands filters
				$scope.parsedFilters.brands = [];
				angular.forEach($scope.appliedFilters.brands, function(value, key){
					if(value == true){
						$scope.parsedFilters.brands.push($scope.filters.brands[key]);
					}
				});

				// prepare data for filters
				if($scope.parsedFilters.products.length){
					$scope.passFilters.productCategories = $scope.parsedFilters.products;
				}
				
				if($scope.parsedFilters.brands.length){
					$scope.passFilters.brands = $scope.parsedFilters.brands;
				}

				if($scope.selectedPriceRange.minPrice || $scope.selectedPriceRange.maxPrice){
					$scope.passFilters.minPriceLimit = $scope.selectedPriceRange.minPrice;
					$scope.passFilters.maxPriceLimit = $scope.selectedPriceRange.maxPrice;
				}
				$scope.getLots();
			};

			$scope.searchWithQuery = function(){				
				$scope.passFilters.queryParam = $scope.searchQueryParam;
				$scope.getLots();
			};

			$scope.addLotToWishlist = function(lot){
				console.log(lot);
				$scope.addToWishlist(lot);
			};
			$scope.removeLotFromWishlist = function(lot){
				console.log(lot);
				$scope.removeFromWishlist(lot);
			};

			$scope.changeSortBy = function(type){
				if(type == "PHL"){
					$scope.sortBy = 'price';
					$scope.sortIsReverse = true;
				}
				if(type == "PLH"){
					$scope.sortBy = 'price';
					$scope.sortIsReverse = false;
				}
			};

		};

	})();