(function() {
	'use strict';
	angular
	.module('app')
	.controller('SellProductStep1Controller', SellProductStep1Controller);

	// SellProductStep1Controller.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	SellProductStep1Controller.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$timeout', 'network'];

	// function SellProductStep1Controller($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function SellProductStep1Controller($scope, $rootScope, $state, $stateParams, $timeout, network) {
			
			// check if user is passing the available category
			// in the url. Redirect to sell if fails
			if($stateParams.category && $stateParams.category.trim() != ''){
				$scope.$parent.selectedCategory = $scope.getSelectedCategory($stateParams.category);
				if($scope.$parent.selectedCategory){
					$scope.getSelectedCategoryId($scope.$parent.selectedCategory, function(){
						$scope.getProductsList = function(){
							network.getProductsInCategory($scope.$parent.selectedCategoryId)
							.then(function(res){
								console.log(res);
								if(res.data.status && res.data.status.toLowerCase() == 'success'){
									$scope.products = res.data.response;
									$scope.$parent.productDetails = res.data.response;
								} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
									swal('Oops...', res.data.message, 'error')
								}
							}, function(err){
								console.log(err);
							});
						};
						$scope.getProductsList();
					});

					$scope.$parent.selectedCategoryIcon = $scope.getSelectedCategoryIcon($stateParams.category);
					// Valid category found
					$scope.selectedProduct = undefined;

					$scope.goToNextStep = function(){
						if($scope.selectedProduct !== null && typeof $scope.selectedProduct === 'object'){
							$scope.$parent.selectedProductId = $scope.selectedProduct._id;
							$state.go('sellProduct.step2');
						} else {
							swal('Oops...', 'Please select one of the product available in the list.', 'error');
						}
					};


				} else {
					$state.go('sell', {}, { reload: true });
				}
			} else {
				$state.go('sell', {}, { reload: true });
			}
		};

	})();