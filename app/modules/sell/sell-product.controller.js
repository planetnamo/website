(function() {
	'use strict';
	angular
	.module('app')
	.controller('SellProductController', SellProductController);

	// SellProductController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	SellProductController.$inject = ['$scope', '$rootScope', '$state', '$timeout', '$stateParams', 'network'];

	// function SellProductController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function SellProductController($scope, $rootScope, $state, $timeout, $stateParams, network) {
			
			$scope.baseImageUrl = 'http://urbanriwaaz.com:8080/api/getFile/baseProductPicture/';

			if (!$scope.user.id) {
				swal("Oops...", "To sell, you are required to login", "warning");
				$state.go("home", {}, {
					reload: true
				});
			}

			$scope.getSelectedCategory = function(cat) {
				if(cat == 'appliances'){
					return "HOME APPLIANCE";
				} else if(cat == 'imac'){
					return "I MAC";
				} else if(cat == 'gaming'){
					return "GAMING CONSOLE";
				} else if(cat == 'mobile' || cat == 'desktop' || cat == 'tv' || cat == 'tablet' || cat == 'laptop'){
					return cat.toUpperCase();
				} else {
					return false;
				}
			};

			$scope.getSelectedCategoryIcon = function(cat) {
				if(cat == 'appliances'){
					return "home-appliances";
				} else if(cat == 'imac'){
					return "i-mac";
				} else if(cat == 'gaming'){
					return "gaming-console";
				} else if(cat == 'mobile' || cat == 'desktop' || cat == 'tv' || cat == 'tablet' || cat == 'laptop'){
					return cat;
				} else {
					return false;
				}
			};

			$scope.selectedCategory = null;
			$scope.selectedCategoryIcon = null;
			$scope.selectedProductId = null;
			// $scope.sellerLocation = null;
			$scope.productDetails = null;
			$scope.workingCondition = null;
			$scope.problemslistavail = null;
			$scope.accessorieslistavail = null;
			$scope.ageinmonths = null;
			$scope.condition = null;
			$scope.validbill = null;
			$scope.originalbox = null;
			$scope.sellRequestCompleted = null;
			$scope.getSelectedCategoryId = function(category,cb){
				var prepareData = {};
				prepareData.filters = {};
				prepareData.filters.queryParam = category;
				network.getProductCategories(prepareData)
				.then(function(res){
					console.log(res);
					if(res.data.status && res.data.status.toLowerCase() == 'success'){
						$scope.selectedCategoryId = res.data.response[0]._id;
						if(cb){
							cb();
						}
					} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
						swal('Oops...', res.data.message, "error");
					}
				}, function(err){
					console.log(err);
				});
			};

		};

	})();