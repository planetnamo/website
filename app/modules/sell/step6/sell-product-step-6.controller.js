(function() {
	'use strict';
	angular
	.module('app')
	.controller('SellProductStep6Controller', SellProductStep6Controller);

	// SellProductStep6Controller.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	SellProductStep6Controller.$inject = ['$scope', '$rootScope', '$state', '$timeout'];

	// function SellProductStep6Controller($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function SellProductStep6Controller($scope, $rootScope, $state, $timeout) {
			
			if($scope.$parent.selectedProductId){
				$scope.$parent.ageinmonths = null;
				$scope.checklist = [
					{
						"name": "Below 3 months",
						"description": "Valid bill required"
					},
					{
						"name": "Below 6 months",
						"description": "Valid bill required"
					},
					{
						"name": "Below 11 months",
						"description": "Valid bill required"
					},
					{
						"name": "Above 11 months",
						"description": ""
					}
				];


				$scope.months = [3,6,11,12];

				$scope.selected = {};
				$scope.selected.months = 3;

				$scope.goToNext = function(){
					console.log("age in months", $scope.selected.months);
					if($scope.selected && $scope.selected.months){
						$scope.$parent.ageinmonths = $scope.selected.months;
						$state.go("sellProduct.step7");
					} else {
						swal("Oops...", "Please select an option.", "error");
					}
				};
			} else {
				$state.go('sell', {}, {reload:true});
			}

		};

	})();