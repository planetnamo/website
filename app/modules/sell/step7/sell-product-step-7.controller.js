(function() {
	'use strict';
	angular
	.module('app')
	.controller('SellProductStep7Controller', SellProductStep7Controller);

	// SellProductStep7Controller.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	SellProductStep7Controller.$inject = ['$scope', '$rootScope', '$state', '$timeout'];

	// function SellProductStep7Controller($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function SellProductStep7Controller($scope, $rootScope, $state, $timeout) {
			
			if($scope.$parent.selectedProductId){
				$scope.$parent.condition = null;
				$scope.activeOption = [];

				$scope.checklist = [
					{
						"name": "Good",
						"value": 1
					},
					{
						"name": "Average",
						"value": 2 
					},
					{
						"name": "Below Average",
						"value": 3
					}
				];

				console.log($scope.$parent.productDetails);
				$scope.checklist[0].description = $scope.$parent.productDetails.goodRemark;
				$scope.checklist[1].description = $scope.$parent.productDetails.averageRemark;
				$scope.checklist[2].description = $scope.$parent.productDetails.belowAverageRemark;

				$scope.selectOption = function(option){
					$scope.activeOption = [];
					$scope.activeOption[option.name] = "active";
					$scope.condition = option.value;
				}

				$scope.goToNext = function(){
					console.log("condition", $scope.condition);
					if($scope.condition){
						$scope.$parent.condition = $scope.condition;
						$state.go("sellProduct.step8");
					} else {
						swal("Oops...", "Please select an option.", "error");
					}
				}
			} else {
				$state.go('sell', {}, {reload:true});
			}
			
		};

	})();