(function() {
	'use strict';
	angular
	.module('app')
	.controller('SellProductStep9Controller', SellProductStep9Controller);

	// SellProductStep9Controller.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	SellProductStep9Controller.$inject = ['$scope', '$rootScope', '$state', '$timeout'];

	// function SellProductStep9Controller($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function SellProductStep9Controller($scope, $rootScope, $state, $timeout) {
			$scope.userAuthRequired();

			if($scope.$parent.sellRequestCompleted){
				$scope.$parent.selectedProductId = null;
				$scope.orderInfo = $scope.$parent.sellRequestCompleted;
				$scope.goToDashboard = function(){
					$scope.$parent.sellRequestCompleted = null;
					$state.go("me.sales", {}, {reload: true});
				};
			} else {
				$state.go("sell", {}, {reload: true});
			}
		};

	})();