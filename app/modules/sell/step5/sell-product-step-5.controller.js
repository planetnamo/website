(function() {
	'use strict';
	angular
	.module('app')
	.controller('SellProductStep5Controller', SellProductStep5Controller);

	// SellProductStep5Controller.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	SellProductStep5Controller.$inject = ['$scope', '$rootScope', '$state', '$timeout'];

	// function SellProductStep5Controller($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function SellProductStep5Controller($scope, $rootScope, $state, $timeout) {
			
			if($scope.$parent.selectedProductId){
				$scope.$parent.accessorieslistavail = null;
				$scope.activeOption = [];

				$scope.accessories = $scope.$parent.productDetails.accessoriesList;

				$scope.accessoriesList = [];
				angular.forEach($scope.accessories, function(value, key){
					$scope.accessoriesList[key] = false;
				});

				// pass $scope.accessoriesList as 'accessorieslistavail'

				$scope.validBill = false;
				$scope.originalBox = false;
				$scope.goToNext = function(){
					console.log("accessories",$scope.accessoriesList);
					console.log("bill and box",$scope.validBill, $scope.originalBox);
					$scope.$parent.validbill = $scope.validBill;
					$scope.$parent.originalbox = $scope.originalBox;
					$scope.$parent.accessorieslistavail = $scope.accessoriesList;
					$state.go("sellProduct.step6");	
				};	
			} else {
				$state.go('sell', {}, {reload:true});
			}

		};

	})();