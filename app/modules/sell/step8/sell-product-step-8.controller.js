(function() {
	'use strict';
	angular
	.module('app')
	.controller('SellProductStep8Controller', SellProductStep8Controller);

	// SellProductStep8Controller.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	SellProductStep8Controller.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'validationService', 'network'];

	// function SellProductStep8Controller($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function SellProductStep8Controller($scope, $rootScope, $state, $timeout, validationService, network) {
			
			if($scope.$parent.selectedProductId){
				// For date picker 
				$scope.today = function() {
					$scope.dt = new Date();
					$scope.dtEnd = new Date();
				};
				$scope.today();

				$scope.clear = function() {
					$scope.dt = null;
				};

				$scope.dateOptions = {
					formatYear: 'yy',
					showWeeks: false,
					maxDate: new Date(2020, 1, 1),
					minDate: new Date(),
					startingDay: 1
				};

				$scope.open1 = function() {
					$scope.popup1.opened = true;
				};

				$scope.popup1 = {
					opened: false
				};

				$scope.open2 = function() {
					$scope.popup2.opened = true;
				};

				$scope.popup2 = {
					opened: false
				};

				$scope.format = "yyyy-MM-dd";

				// For time picker
				// $scope.time = new Date();
				// $scope.timeEnd = new Date();
				// $scope.hstep = 1;
				// $scope.mstep = 1;
				// $scope.ismeridian = true;

				$scope.selectedAddressId = null;
				$scope.activeAddress = [];
				$scope.selectAddress = function(addressId){
					$scope.activeAddress = [];
					$scope.activeAddress[addressId] = 'active';
					$scope.selectedAddressId = addressId;
				};

				$scope.addNewAddress = function(){
					$scope.viewSavedAddresses = false;
					$scope.activeAddress = [];
					$scope.selectedAddressId = null;
				};
				$scope.showSavedAddresses = function(){
					$scope.viewSavedAddresses = true;
				};

				$scope.showSchedulePickupModal = function(){
					$('.schedule-pickup-modal').modal('toggle');
					$scope.loadingAddress = true;
					network.getUserInfo($scope.user.authToken)
					.then(function(res){
						if(res.data.status && res.data.status.toLowerCase() == 'success'){
							$scope.userAddress = res.data.response.address;
							$scope.userMobile = res.data.response.mobile;
							$scope.showSavedAddresses();
						} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
							swal('Oops...', res.data.message, 'error');
						}
						$scope.loadingAddress = false;
					}, function(err){
						if(err.status == 401){
							swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
					});
				};

				$scope.address = {};
				$scope.address.country = "India";
				$scope.isAddressValid = function(){
					if($scope.address.line1 && $scope.address.line1.trim() != ""){
						// prepareAddress.line1 = $scope.address.line1;
					} else {
						swal("Oops...", "Please enter your address line 1.", "error");
						return false;
					}
					if($scope.address.city && $scope.address.city.trim() != ""){
						// prepareAddress.city = $scope.address.city;
					} else {
						swal("Oops...", "Please enter your city.", "error");
						return false;
					}
					if($scope.address.state && $scope.address.state.trim() != ""){
						// prepareAddress.state = $scope.address.state;
					} else {
						swal("Oops...", "Please enter your state.", "error");
						return false;
					}
					if($scope.address.country && $scope.address.country.trim() != ""){
						// prepareAddress.country = $scope.address.country;
					} else {
						swal("Oops...", "Please enter your country.", "error");
						return false;
					}
					if($scope.address.zipCode && $scope.address.zipCode.trim() != ""){
						// prepareAddress.zipCode = $scope.address.zipCode;
					} else {
						swal("Oops...", "Please enter your zip code.", "error");
						return false;
					}
					// prepareAddress.line2 = $scope.address.line2;
					// prepareAddress.locality = $scope.address.locality;
					return true;
				};


				$scope.sendConfirmRequest = function(requestData){
					network.createSellingOrder($scope.user.authToken, requestData)
					.then(function(res){
						console.log(res);
						if(res.data.status && res.data.status.toLowerCase() == 'success'){
							$scope.$parent.sellRequestCompleted = res.data.response;
							$('.schedule-pickup-modal').modal('hide');
							// Timeout is used to hide modal first
							// before state redirect
							$timeout(function(){$state.go('sellProduct.step9');},500);
						} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
							swal("Oops...", res.data.message, "error");
						}
					}, function(err){
						console.log(err);
					});
				};

				$scope.confirmPickup = function(){
					var requestData = {};
					if($scope.selectedAddressId){
						if($scope.mobileNumber && $scope.mobileNumber.trim() != ""){
							if(validationService.isPhoneValid($scope.mobileNumber)){
								requestData.productId = $scope.newProduct._id;
								requestData.schedulePickUpDate = moment($scope.dt).format("YYYY-MM-DD");
								requestData.addressId = $scope.selectedAddressId;
								requestData.mobileNumber = $scope.mobileNumber;
								$scope.sendConfirmRequest(requestData);
							} else {
								swal("Oops...", "Please enter a valid mobile number.", "error");
								return false;
							}
						} else {
							swal("Oops...", "Please enter your mobile number.", "error");
							return false;
						}
					} else if(!$scope.viewSavedAddresses && $scope.isAddressValid()){
						if($scope.mobileNumber && $scope.mobileNumber.trim() != ""){
							if(validationService.isPhoneValid($scope.mobileNumber)){
								requestData.productId = $scope.newProduct._id;
								requestData.schedulePickUpDate = moment($scope.dt).format("YYYY-MM-DD");
								requestData.address = $scope.address;
								requestData.mobileNumber = $scope.mobileNumber;
								$scope.sendConfirmRequest(requestData);
							} else {
								swal("Oops...", "Please enter a valid mobile number.", "error");
								return false;
							}
						} else {
							swal("Oops...", "Please enter your mobile number.", "error");
							return false;
						}
					} else if($scope.viewSavedAddresses){
						swal("Oops...", "Please select or provide a valid address", "error");
					}
				};

				$scope.prepareData = {
					"baseProductId": $scope.$parent.selectedProductId,
					"ageinmonths": $scope.$parent.ageinmonths,
					"condition": $scope.$parent.condition,
					"validbill": $scope.$parent.validbill,
					"originalbox": $scope.$parent.originalbox,
					"workingcondition": $scope.$parent.workingCondition,
					"accessorieslistavail": $scope.$parent.accessorieslistavail,
					"problemslistavail": $scope.$parent.problemslistavail
				};
				console.clear();
				console.log($scope.prepareData);

				$scope.getEstimatedPrice = function(){
					network.createNewProduct($scope.user.authToken, $scope.prepareData)
					.then(function(res){
						console.log(res);
						if(res.data.status && res.data.status.toLowerCase() == 'success'){
							$scope.newProduct = res.data.response;
						} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
							swal("Oops...", res.data.message, 'error');
							// $state.go('sell', {}, {reload: true});
						}
					}, function(err){
						console.log(err);
					});
				};
				$scope.getEstimatedPrice();
			} else {
				$state.go('sell', {}, {reload:true});
			}

	};

})();