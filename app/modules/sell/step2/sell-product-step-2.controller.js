(function() {
	'use strict';
	angular
	.module('app')
	.controller('SellProductStep2Controller', SellProductStep2Controller);

	// SellProductStep2Controller.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	SellProductStep2Controller.$inject = ['$scope', '$rootScope', '$state', '$timeout', 'network'];

	// function SellProductStep2Controller($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function SellProductStep2Controller($scope, $rootScope, $state, $timeout, network) {
			if($scope.$parent.selectedProductId){
				// Config for google auto complete places
				$scope.autoCompleteOptions = {
					componentRestrictions: { country: 'in' }
				};
				
				$scope.getSellingProductSpecs = function(){
					$scope.loadingData = true;
					network.getSellingProductSpecs($scope.$parent.selectedProductId)
					.then(function(res){
						console.log(res);
						if(res.data.status && res.data.status.toLowerCase() == 'success'){
							$scope.$parent.productDetails = res.data.response;
						} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
							swal('Oops...', res.data.message, 'error');
						}
						$scope.loadingData = false;
					}, function(err){
						console.log(err);
					});
				}();

				// $scope.getSellerAddress = function(){
				// 	if($scope.location){
				// 		if($scope.location.address_components && $scope.location.address_components[0].longName){
				// 			return $scope.location.address_components[0].longName;
				// 		} else if($scope.location && $scope.location.trim() != ''){
				// 			return $scope.location;
				// 		} else {
				// 			return false;
				// 		}
				// 	} else {
				// 		return false;
				// 	}
				// };
				$scope.goToNext = function(){
					// if($scope.location){
						// $scope.$parent.sellerLocation = $scope.location;
						$state.go("sellProduct.step3");
					// } else {
						// swal("Oops...", "Please input your location.", "error");
					// }
				}
			} else {
				$state.go('sell', {}, {reload:true});
			}

		};

	})();