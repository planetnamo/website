(function() {
	'use strict';
	angular
	.module('app')
	.controller('SellProductStep4Controller', SellProductStep4Controller);

	// SellProductStep4Controller.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	SellProductStep4Controller.$inject = ['$scope', '$rootScope', '$state', '$timeout'];

	// function SellProductStep4Controller($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function SellProductStep4Controller($scope, $rootScope, $state, $timeout) {
			
			if($scope.$parent.selectedProductId){
				$scope.$parent.problemslistavail = null;
				$scope.selectedOption = null;
				$scope.activeOption = [];

				$scope.problems = $scope.$parent.productDetails.problemsList;

				$scope.problemsList = [];
				angular.forEach($scope.problems, function(value, key){
					$scope.problemsList[key] = false;
				});

				// pass $scope.problemsList as 'problemslistavail'
				$scope.goToNext = function(){
					console.log("problems list", $scope.problemsList);
					
					$scope.$parent.problemslistavail = $scope.problemsList;
					$state.go("sellProduct.step5");
				};
			} else {
				$state.go('sell', {}, {reload:true});
			}
		};

	})();