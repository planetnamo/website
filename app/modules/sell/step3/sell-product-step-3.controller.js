(function() {
	'use strict';
	angular
	.module('app')
	.controller('SellProductStep3Controller', SellProductStep3Controller);

	// SellProductStep3Controller.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	SellProductStep3Controller.$inject = ['$scope', '$rootScope', '$state', '$timeout'];

	// function SellProductStep3Controller($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function SellProductStep3Controller($scope, $rootScope, $state, $timeout) {
			
			if($scope.$parent.selectedProductId){
				$scope.activeOption = [];

				$scope.selectOption = function(option){
					if(option == 'yes'){
						$scope.activeOption = [];
						$scope.activeOption['yes'] = "active";
						$scope.$parent.workingCondition = true;
					} else if(option == 'no') {
						$scope.activeOption = [];
						$scope.activeOption['no'] = "active";
						$scope.$parent.workingCondition = false;
					} else {

					}
				};

				if($scope.$parent.workingCondition == true){
					$scope.selectOption('yes');
				} else if($scope.$parent.workingCondition == false){
					$scope.selectOption('no');
				}

				$scope.goToNext = function(){
					if($scope.$parent.workingCondition != null){
						console.log("working condition", $scope.$parent.workingCondition);
						$state.go("sellProduct.step4");
					} else {
						swal("Oops...", "Please select an option.", "error");
					}
				};	
			} else {
				$state.go('sell', {}, {reload:true});
			}
		};

	})();