(function() {
	'use strict';
	angular
	.module('app')
	.controller('DetailsDirectBuyController', DetailsDirectBuyController);

	// DetailsDirectBuyController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	DetailsDirectBuyController.$inject = ['$scope', '$rootScope', '$state', '$timeout'];

	// function DetailsDirectBuyController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function DetailsDirectBuyController($scope, $rootScope, $state, $timeout) {
			
			$scope.navTabs = [
			{
				"name":"SPECIFICATIONS"	
			},
			{
				"name":"FEATURES"	
			},
			{
				"name":"WARRANTY"	
			},
			{
				"name":"T&C'S"	
			}
			];

			$scope.activeTabs = [];
			$scope.activeTabs['SPECIFICATIONS'] = "active"; 

			$timeout(function(){
				$('.images-wrapper .images-big').slick({
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					fade: true,
					asNavFor: '.images-wrapper .images-thumb'
				});

				$('.images-wrapper .images-thumb').slick({
					slidesToShow: 3,
					slidesToScroll: 1,
					asNavFor: '.images-wrapper .images-big',
					dots: false,
					arrows:true,
					centerMode: true,
					focusOnSelect: true,
					prevArrow: '<button type="button" class="slick-prev pn-gradient custom custom-left">Previous</button>',
					nextArrow: '<button type="button" class="slick-next pn-gradient custom custom-right">Next</button>',
					responsive: [
					{
						breakpoint: 991,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 767,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}
					]
				});
			},500);
		};

	})();