(function() {
	'use strict';
	angular
	.module('app')
	.controller('CartRentalController', CartRentalController);

	// CartRentalController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	CartRentalController.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$timeout', 'network'];

	// function CartRentalController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function CartRentalController($scope, $rootScope, $state, $stateParams, $timeout, network) {
			$scope.userAuthRequired();
			console.log($stateParams);
			if($stateParams.lotId && $stateParams.lotId != ""){
				// Get product detail

				$scope.monthsToRent = $stateParams.monthsToRent;
				$scope.startRentalDate = $stateParams.startRentalDate;
				$scope.calculatedRental = $stateParams.rent;

				$scope.resetAddress = function(){
					$scope.selectedAddressId = null;
					$scope.activeAddress = [];
					$scope.selectAddress = function(addressId){
						$scope.activeAddress = [];
						$scope.activeAddress[addressId] = 'active';
						$scope.selectedAddressId = addressId;
					};
				}
				$scope.resetAddress();

				$scope.getLotDetail = function(lotId, cb){
					$scope.loadingData = true;
					network.getLotDetail($scope.user.authToken, lotId)
					.then(function(res){
						console.log(res);
						if(res.data.status && res.data.status.toLowerCase() == 'success'){
							$scope.lot = res.data.response;

							$scope.securityDeposit = $scope.lot.lotItemDetails[0].rentalSecurityDeposit * $scope.lot.lotItemDetails[0].quantityAvailable,
							$scope.totalPayable = $scope.securityDeposit + $scope.calculatedRental,
							$scope.orderDetails = {};
							$scope.orderDetails[$scope.lot.lotItemDetails[0].productId] = $scope.lot.lotItemDetails[0].quantityAvailable;
							if(cb){
								cb();
							}
						} else {
							// $state.go('home', {}, {reload: true});
						}
					}, function(err){
						console.log(err);
						if(err.status == 401){
							$scope.logoutUser();
							// swal("Unauthorized", "You are not authorized to perform this action", "error");
						} else {
							swal("Oops...", "Something went wrong. Please try again.", "error");
						}
						$scope.loadingData = false;
					});
				};

				// Get user address
				$scope.getUserInfo = function() {
					$scope.loadingAddresses = true;
					network.getUserInfo($scope.user.authToken)
						.then(function(res) {
							if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
								$scope.userInfo = res.data.response;
								$scope.resetAddress();
								$scope.loadingAddresses = false;
								$scope.loadingData = false;
							} else if (res.data && res.data.status && res.data.status == "error") {
								swal("Oops...", res.data.message, "error");
							}
							console.log($scope.userInfo);
						}, function(err) {
							if(err.status == 401){
								$scope.logoutUser();
								// swal("Unauthorized", "You are not authorized to perform this action", "error");
							} else {
								swal("Oops...", "Something went wrong. Please try again.", "error");
							}
						});
				};

				$scope.getLotDetail($stateParams.lotId, function(){
					$scope.getUserInfo();
				});

				$scope.isAddressValid = function(){
					if($scope.address.line1 && $scope.address.line1.trim() != ""){
						// prepareAddress.line1 = $scope.address.line1;
					} else {
						swal("Oops...", "Please enter your address line 1.", "error");
						return false;
					}
					if($scope.address.city && $scope.address.city.trim() != ""){
						// prepareAddress.city = $scope.address.city;
					} else {
						swal("Oops...", "Please enter your city.", "error");
						return false;
					}
					if($scope.address.state && $scope.address.state.trim() != ""){
						// prepareAddress.state = $scope.address.state;
					} else {
						swal("Oops...", "Please enter your state.", "error");
						return false;
					}
					if($scope.address.country && $scope.address.country.trim() != ""){
						// prepareAddress.country = $scope.address.country;
					} else {
						swal("Oops...", "Please enter your country.", "error");
						return false;
					}
					if($scope.address.zipCode && $scope.address.zipCode.trim() != ""){
						// prepareAddress.zipCode = $scope.address.zipCode;
					} else {
						swal("Oops...", "Please enter your zip code.", "error");
						return false;
					}
					return true;
				};

				$scope.address = {};
				$scope.address.country = 'India';
				$scope.addAddress = function(){
					if($scope.isAddressValid()){
						$scope.loadingInModal = true;

						network.addAddress($scope.user.authToken, $scope.address)
						.then(function(res){
							if(res.data.status && res.data.status.toLowerCase() == 'success'){
								$scope.address = {};
								$scope.address.country = 'India';
								$scope.resetAddress();
								$scope.loadingInModal = false;
								$('.modal.add-new-address-modal').modal('hide');
								$scope.getUserInfo();	
							} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
								swal('Oops...', res.data.message, 'error');
							}
						}, function(err){
							if(err.status == 401){
								swal("Unauthorized", "You are not authorized to perform this action", "error");
							} else {
								swal("Oops...", "Something went wrong. Please try again.", "error");
							}
						});
					}
				};

				$scope.removeAddress = function(address, event){
					event.preventDefault();
					event.stopPropagation();
					
					if($scope.isAddressValid()){
						$scope.loadingInModal = true;

						network.editAddress($scope.user.authToken, $scope.address)
						.then(function(res){
							if(res.data.status && res.data.status.toLowerCase() == 'success'){
								$scope.address = {};
								$scope.address.country = 'India';
								$scope.resetAddress();
								$scope.loadingInModal = false;
								$('.modal.add-new-address-modal').modal('hide');
								$scope.getUserInfo();	
							} else if(res.data.status && res.data.status.toLowerCase() == 'error'){
								swal('Oops...', res.data.message, 'error');
							}
						}, function(err){
							if(err.status == 401){
								swal("Unauthorized", "You are not authorized to perform this action", "error");
							} else {
								swal("Oops...", "Something went wrong. Please try again.", "error");
							}
						});
					}
				};

				$scope.editAddress = function(){
					if($scope.isAddressValid()){
						$scope.loadingInModal = true;

						network.editAddress($scope.user.authToken, $scope.address, $scope.editAddressId)
						.then(function(res){
							console.log(res);
							if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
								swal("Updated!", "Selected address has been updated.", "success");
								$scope.address = {};
								$scope.address.country = 'India';
								$scope.resetAddress();
								$scope.loadingInModal = false;
								$scope.editAddressId = null;
								$('.modal.add-new-address-modal').modal('hide');
								$scope.getUserInfo();
							} else if (res.data && res.data.status && res.data.status == "error") {
								swal("Oops...", res.data.message, "error");
							}
						}, function(err){
							if(err.status == 401){
								$scope.logoutUser();
							} else {
								swal("Oops...", "Something went wrong. Please try again.", "error");
							}
						});
					}
				};

				$scope.showEditAddress = function(address, event){
					event.preventDefault();
					event.stopPropagation();
					
					$scope.showEditButton = true;
					$scope.address = angular.copy(address);
					$scope.editAddressId = address._id;
					$('.modal.add-new-address-modal').modal('toggle');
				};

				$scope.showAddNewAddressForm = function(){
					$scope.showEditButton = false;
					$('.modal.add-new-address-modal').modal('toggle');
				};

				$scope.paymentConfigAddToWallet = {
					"key": $scope.razorPayKey,
					"name": "PlanetNamo Wallet",
					"description": "Wallet Recharge",
					"image": "assets/imgs/planet-namo-logo.png",
					"handler": function(response) {
						$scope.razorpayResponseAddToWallet(response);
					},
					"prefill": {
					},
					"theme": {
						"color": "#3bb549"
					}
				};

				$scope.razorpayResponseAddToWallet = function(response){
					if (response.razorpay_payment_id) {
						$scope.addingAmountToWallet = true;
						var apiData = {
							"razorpayPaymentID": response.razorpay_payment_id,
							"amount": parseInt(parseFloat($scope.amountNeededMore) * 100) // Converting to paise
						};

						$scope.paymentLoading = true;
						
						network.addAmountToWallet($scope.user.authToken, apiData)
						.then(function(res) {
							// console.log(res);
							if (res.data && res.data.status && res.data.status.toLowerCase() == "success") {
								var postData = {};
								postData.lotId = $scope.lot.lotDetails.lotId;
								postData.monthsToRent = $scope.monthsToRent;
								postData.startRentalDate = $scope.startRentalDate;
								postData.orderDetails = {};
								postData.orderDetails[$scope.lot.lotItemDetails[0].productId] = $scope.lot.lotItemDetails[0].quantityAvailable;
								postData.deliveryAddressId = $scope.selectedAddressId;

								network.placeRentalOrder($scope.user.authToken, postData)
								.then(function(res){
									console.log(res);
									if(res.data.status && res.data.status.toLowerCase() == 'success'){
										swal("Success", "Order Placed Successfully.", "success");
										$state.go("me.rentals",{},{reload: true});
									}else if(res.data.status && res.data.status.toLowerCase() == 'error' && res.data.amountNeededMore){
										swal("Alert", "", "success");
									} else if (res.data && res.data.status && res.data.status == "error") {
										swal("Oops...", res.data.message, "error");
									}else{
										swal("Oops...", "Unable to reach servers. Please Try Again.", "error");
									}
									$scope.paymentLoading = false;
									$scope.amountNeededMore = 0;
								}, function(err){
									console.log(err);
									if(err.status == 401){
										$scope.logoutUser();
										// swal("Unauthorized", "You are not authorized to perform this action", "error");
									} else {
										swal("Oops...", "Something went wrong. Please try again.", "error");
									}
									$scope.paymentLoading = false;
									$scope.amountNeededMore = 0;
								});
							} else if(res.data && res.data.error){
								swal("Oops...", res.data.error, "error");
								$scope.paymentLoading = false;
								$scope.amountNeededMore = 0;
							}else{
								swal("Oops...", "Unable to add amount to your wallet. Please contact our customer care", "error");
								$scope.paymentLoading = false;
								$scope.amountNeededMore = 0;
							}
						}, function(err) {
							swal("Oops...", "Something went wrong while processing your payment.", "error");
							$scope.paymentLoading = false;
							$scope.amountNeededMore = 0;
						});
					} else if (response.error_code) {
						// console.log(response.error_code);
						$scope.amountNeededMore = 0;
						$scope.paymentLoading = false;
						swal("Oops...", "Unable to process your payment request. Please try again.", "error");
					}
				}

				$scope.proceedToPay = function(){
					if($scope.selectedAddressId){
						swal({
						  title: "Are you sure?",
						  text: "Amount of " + ($scope.totalPayable) + " will be deducted from your wallet.",
						  type: "warning",
						  showCancelButton: true,
						  confirmButtonColor: "#5cb85c",
						  confirmButtonText: "Yes, Continue",
						  cancelButtonText: "No",
						  closeOnConfirm: true,
						  closeOnCancel: true
						},
						function(isConfirm){
						  if (isConfirm) {
						    $scope.paymentLoading = true;

							var postData = {};
							postData.lotId = $scope.lot.lotDetails.lotId;
							postData.monthsToRent = $scope.monthsToRent;
							postData.startRentalDate = $scope.startRentalDate;
							postData.orderDetails = {};
							postData.orderDetails[$scope.lot.lotItemDetails[0].productId] = $scope.lot.lotItemDetails[0].quantityAvailable;
							postData.deliveryAddressId = $scope.selectedAddressId;
							
							network.placeRentalOrder($scope.user.authToken, postData)
							.then(function(res){
								// console.log(res);
								if(res.data.status && res.data.status.toLowerCase() == 'success'){
									swal("Success", "Order Placed Successfully.", "success");
									$state.go("me.rentals",{},{reload: true});
								}else if(res.data.status && res.data.status == "error" && res.data.amountNeededMore){
									swal({
									  title: "Not sufficient amount in wallet",
									  text: "Recharge wallet with "+res.data.amountNeededMore+" more inorder to continue.",
									  type: "warning",
									  showCancelButton: true,
									  confirmButtonColor: "#5cb85c",
									  confirmButtonText: "Yes, Continue",
									  cancelButtonText: "No",
									  closeOnConfirm: true,
									  closeOnCancel: true
									},
									function(isConfirm){
									  if (isConfirm) {
									    $scope.paymentLoading = true;
									    $scope.paymentConfigAddToWallet.amount = parseInt(parseFloat(res.data.amountNeededMore) * 100); //Converting to paise
										$scope.paymentConfigAddToWallet.prefill.name = $scope.user.name;
										$scope.paymentConfigAddToWallet.prefill.contact = $scope.user.mobile;
										$scope.paymentConfigAddToWallet.prefill.mobile = $scope.user.mobile;
										$scope.paymentConfigAddToWallet.prefill.email = $scope.user.email;
										$scope.instance = new Razorpay($scope.paymentConfigAddToWallet);
										$scope.amountNeededMore = res.data.amountNeededMore;
										$scope.instance.open();
									  } else {
									  }
									});
								} else if (res.data && res.data.status && res.data.status == "error") {
									swal("Oops...", res.data.message, "error");
								}else{
									swal("Oops...", "Unable to reach servers. Please Try Again.", "error");
								}
								$scope.paymentLoading = false;
							}, function(err){
								console.log(err);
								if(err.status == 401){
									$scope.logoutUser();
									// swal("Unauthorized", "You are not authorized to perform this action", "error");
								} else {
									swal("Oops...", "Something went wrong. Please try again.", "error");
								}
								$scope.paymentLoading = false;
							});
						  } else {
						  }
						});
					}else{
						swal("Oops...","Select the address for delivery", "warning")
					}
				}

			} else {
				// $state.go("home");
			}
		};

	})();