(function() {
	'use strict';
	angular
	.module('app')
	.controller('CartOverviewController', CartOverviewController);

	// CartOverviewController.$inject = ['$scope', '$rootScope', '$state', '$localStorage', 'Facebook', 'network', '$location'];
	CartOverviewController.$inject = ['$scope', '$rootScope', '$state', '$timeout'];

	// function CartOverviewController($scope, $rootScope, $state, $localStorage, Facebook, network, $location) {
		function CartOverviewController($scope, $rootScope, $state, $timeout) {
			if($scope.fromParams){
				$state.go($scope.fromState, $scope.fromParams);
			} else {
				$state.go($scope.fromState);
			}
			
		};

	})();